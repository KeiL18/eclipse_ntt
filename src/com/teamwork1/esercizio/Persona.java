package esercitazione3;

import java.util.Date;

public class Persona {
	
	protected String nome;
	protected String cognome;
	//private Date nascita;
	
	Persona(){	
	}
	Persona(String var_nome,String var_cognome, Date var_nascita)
	{
		this.nome=var_nome;
		this.cognome=var_cognome;
		//this.nascita=var_nascita;
	}
	public void setNome(String var_nome) {
		this.nome=var_nome;
	}
	public void setCognome(String var_cognome) {
		this.cognome=var_cognome;
	}
	//public void setNascita(Date var_nascita) {
	//	this.nascita=var_nascita;
	//}
	public String getNome() {
		return this.nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	//public Date getNascita() {
		//return this.nascita;
	//}
	//public void stampaPersona() {
		//System.out.println(this.nome+" "+this.cognome+" "+this.nascita +"\n");
	//}
	public void stampaPersona() {
		System.out.println(this.nome+" "+this.cognome);
	}
}
