package com.teamwork1.soluz;

public abstract class Persona {
	
	protected String nome;
	protected String cognome;
	
	public void setNome(String var_nome) {
		this.nome = var_nome;
	}
	public void setCognome(String var_cognome) {
		this.cognome = var_cognome;
	}
	
	public String getNome() {
		return this.nome;
	}
	public String getCognome() {
		return this.cognome;
	}
}
