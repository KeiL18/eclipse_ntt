package com.teamwork1.soluz;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Figlio fig_1 = new Figlio("Giovanni", "Pace");
		Figlio fig_2 = new Figlio("Mario", "Rossi");
		
		Genitore gen_1 = new Genitore("Maria", "Mattarella");
		gen_1.addFiglio(fig_1);
		gen_1.addFiglio(fig_2);
		gen_1.stampaElenco();
		
		Genitore gen_2 = new Genitore("Mariolino", "Rossellino");
		gen_2.addFiglio(fig_2);
		gen_2.stampaElenco();
		
	}

}