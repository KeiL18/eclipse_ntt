package com.lezione1.stringhe;

public class Stringhe {

    public static void main(String[] args) {

        //System.out.println("Ciao sono Giovanni");

        /*
        String frase = "Ciao sono Giovanni";    //Stringa come variabile
        System.out.println(frase);
        */

        //System.out.println("CIAO SONO " + "Giovanni");

        /*
        String nome = "Giovanni";
        String cognome = "Pace";

        //System.out.println(nome + ", " + cognome);

        String frase = nome + ", " + cognome;
        System.out.println(frase);
        */

        /* -------------------------- */

        /*
        int a, b;
        a = 5;
        b = 8;

        //System.out.println(a + b);
        //System.out.println("Il risultato �: " + a + b);
        //System.out.println(a + b + " � il risultato");
        //System.out.println("Il risultato �: " + (a + b));

        //int somma = a + b + " � il risultato";    //NO BUENO!
         */

        /*
         * Giovanni Pace � 10 anni vecchio ed ha una temperatura corporea
         * di 36.6 gradi.
         * Mario Rossi � 20 anni vecchio ed ha una temperatura corporea
         * di 35.8 gradi.
         *
         * VARIABILI: nominativo - et� - temperatura
         */

        String nominativo = "Giovanni Pace";
        int eta = 10;
        float temp = 36.6f;

        System.out.println(
                nominativo + " � " + eta +
                " anni vecchio ed ha una temperatura corporea di " + temp + " gradi"
                );

        nominativo = "Mario Rossi";
        eta = 20;
        temp = 35.8f;

        System.out.println(
                nominativo + " � " + eta +
                " anni vecchio ed ha una temperatura corporea di " + temp + " gradi"
                );
    }

}
