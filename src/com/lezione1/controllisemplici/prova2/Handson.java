package com.lezione1.controllisemplici.prova2;

public class Handson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
	     * Verificate tramite variabili booleane e controlli IF senza ramo else la seguente tipologia di operazione
	     * Se hai un'et� >= 18 anni allora sei maggiorenne, altrimenti sei minorenne facendo attenzione ai casi particolari:
	     * et� inferiore a zero e maggiore di 120 anni.
	     */

		int eta = 19;
		boolean eta_valida = true;
		boolean maggiorenne = true;
		
		if (eta < 0 || eta > 120)
		{
			System.out.println("Errore di validit�");
			eta_valida = false;
		}
		
		if (eta_valida)
		{
			if (eta < 18)
				{
				maggiorenne = false;
				}
			
			if (maggiorenne) {
				System.out.println("Maggiorenne");
			}
			
			if (!(maggiorenne)) {
				System.out.println("Minorenne");
			}
		}
		
		
		

	}

}
