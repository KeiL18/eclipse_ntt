package com.lezione1.controlliComplessi.esercizio;

import java.util.Scanner;

public class Handson {

	public static void main(String[] args) {
		
		/**
         * Scrivere un piccolo programma che, dato in input il giorno della settimana (in numero), vi restituisca il nome
         * - 1 = Luned�
         * - 2 = Marted�
         * ...
         *
         * NON RISCRIVENDO LO SWITCH-CASE ;)
         * CHALLENGE... e se volessimo cambiare il sistema di conversione dei giorni nello stile americano?
         * - 1 = Domenica
         * - 2 = Luned�
         * - 3 = Marted�
         * ...
         *
         * CHALLENGE... Se volete, scrivere un programma che all'inizio vi chieda la tipologia di data
         * da convertire (se italiana o americana) e poi effettuare la conversione.
         */
		
		int giorno = 0;
		boolean italiana = true;
		
		Scanner interceptorS = new Scanner(System.in);
		Scanner interceptorN = new Scanner(System.in);
		System.out.println("Indicare la tipologia di data (italiana o americana):");
		String lingua = interceptorS.nextLine();
		
		switch (lingua.toLowerCase()) {
			case "italiana":
				italiana = true;
				break;
			case "americana":
				italiana = false;
				break;
			default:
				System.out.println("Inseririmento tipologia della data errato");
		}
		
		System.out.println("Inserire il giorno:");
		giorno = Integer.parseInt(interceptorN.nextLine());
		
		if(!(italiana))
		{
			giorno = giorno - 1;
			if (giorno == 0)
				giorno = 7;
		}
		
		switch (giorno) {
			case 1:
				System.out.println("Luned�");
				break;
			case 2:
				System.out.println("Marted�");
				break;
			case 3:
				System.out.println("Mercoled�");
				break;
			case 4:
				System.out.println("Gioved�");
				break;
			case 5:
				System.out.println("Venerd�");
				break;
			case 6:
				System.out.println("Sabato");
				break;
			case 7:
				System.out.println("Domenica");
				break;
			default:
				System.out.println("Giorno inesistente");
		}

	}
	
//	interceptorS.close();
//	interceptorN.close();

}
