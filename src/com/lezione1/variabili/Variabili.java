package com.lezione1.variabili;

public class Variabili {

    public static void main(String[] args) {

        /*
         * Definizione delle variabili intere
         */

        /*
        int a = 522;
        int b = 896;

        System.out.println(a);
        System.out.println(b);
        System.out.println(a);
        */

        /* --------------------------- */
        /*
        int a;      //Definizione
        a = 522;    //Assegnazione
        System.out.println(a);

        a = 321;    //Riassegnazione
        System.out.println(a);

        //int a;    //Non � possibile ridichiarare due variabili con lo stesso nome all'interno dello stesso file!
        */

        /* --------------------------- */
        /*
        int a, b;       //Dichiarazione sequenziale delle variabili;

        a = 522;

        b = a;
        System.out.println("Il valore di b �: " + b);

        a = 892;
        System.out.println("Il valore di b �: " + b);   //Il valore di b non cambia perch� si tratta di tipo primitivo ed � comunque 522
        */

        /* --------------------------- */
        /*
        int a, b, somma;
        a = 2147483647;
        b = 896;

        somma = a + b;
        System.out.println("La somma �: " + somma);
        */

        /* --------------------------- */
        float a = 5.2f;
        System.out.println(a);

        double b = 5.2d;        //Possiamo definire anche 5.2f perch� il Double pu� contenere al suo interno dei Float
        System.out.println(b);
    }

}
