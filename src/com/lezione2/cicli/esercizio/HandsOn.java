package com.lezione2.cicli.esercizio;

import java.util.Scanner;

public class HandsOn {

	public static void main(String[] args) {
		/*
         * Creare un sistema di input che abbia come finalit� la creazione di un elenco di studenti.
         * Uno studente � caratterizzato da NOME, COGNOME, MATRICOLA (tutto alfanumerico)
         * Ad ogni inserimento, questi studenti verranno elencati sequenzialmente e stampati in Console.
         *
         * TIP: Create una stringa chiamata Lista Studenti!
         */

		boolean Inserimento = true;
		String ListaStudenti = "", Intro = "\nGli studenti sono:\n";
		
		Scanner interceptor = new Scanner(System.in);
		do {
			System.out.println("Inserire il nome (QUIT per uscire):");
			String info = interceptor.nextLine();
			
			if (info.trim().toUpperCase().equals("QUIT")){
				Inserimento = false;
			}
			else {
				ListaStudenti = ListaStudenti + info + ", ";
				
				System.out.println("Inserire il cognome:");
				info = interceptor.nextLine();
				ListaStudenti = ListaStudenti + info + ", ";
				
				System.out.println("Inserire la matricola:");
				info = interceptor.nextLine();
				ListaStudenti = ListaStudenti + info + "\n";
				
				System.out.println(Intro + ListaStudenti + "\n-----------------------\n");
			}
			
		} while (Inserimento);
		
		System.out.println("TERMINATO\n ---------LISTA FINALE-------");
		System.out.println(Intro + ListaStudenti + "\n-----------------------\n");
		
	}

}
