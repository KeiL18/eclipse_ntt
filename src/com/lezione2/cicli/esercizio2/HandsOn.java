package com.lezione2.cicli.esercizio2;

import java.util.Scanner;

public class HandsOn {

    public static void main(String[] args) {

//      int val_max = 0;
//      int indice = 0;
//
//      while(indice < val_max) {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;           //indice =  indice + 1; / indice += 1;
//      }

        /* ------------------------ */

//      int val_max = 0;
//      int indice = 0;
//
//      do {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;
//
//      } while (indice < val_max);

        /* ------------------------ */

//      Scanner interceptor = new Scanner(System.in);       //Dichiario lo Scanner
//
//      int val_max = 3;
//      int i=0;
//
//      while(i<val_max) {
//          System.out.println("Dimmi qual'� il tuo nome: ");
//          String nome = interceptor.nextLine();
//          System.out.println("Ciao " + nome);
//
//          i++;
//      }
//
//      interceptor.close();

        /* ------------------------ */

//      Scanner interceptor = new Scanner(System.in);
//
//      boolean scrittura_abilitata = true;
//
//      while(scrittura_abilitata) {
//          System.out.println("Dimmi qual'� il tuo nome (digita QUIT per uscire): ");
//          String nome = interceptor.nextLine();
//
//          if(nome.equals("QUIT")) {
//              scrittura_abilitata = false;
//          }
//          else {
//              System.out.println("Ciao " + nome);
//          }
//      }
//
//      System.out.println("Programma terminato");
//
//      interceptor.close();

        /*
         * Creare un sistema di input che abbia come finalit� la creazione di un elenco di studenti.
         * Uno studente � caratterizzato da NOME, COGNOME, MATRICOLA (tutto alfanumerico)
         * Ad ogni inserimento, questi studenti verranno elencati sequenzialmente e stampati in Console.
         *
         * TIP: Create una stringa chiamata Lista Studenti!
         */

//        Scanner interceptor = new Scanner(System.in);
//
//        boolean inserimento_abilitato = true;
//        String lista_studenti = "";
//
//        while(inserimento_abilitato) {
//            System.out.println("Dimmi qual'� il tuo nome (digita QUIT per uscire): ");
//            String nome = interceptor.nextLine();
//            if(nome.equals("QUIT")) {
//                inserimento_abilitato = false;
//            }
//            else {
//                System.out.println("Dimmi qual'� il tuo cognome (digita QUIT per uscire): ");
//                String cognome = interceptor.nextLine();
//                if(cognome.equals("QUIT")) {
//                    inserimento_abilitato = false;
//                }
//                else {
//                    System.out.println("Dimmi qual'� la tua matricola (digita QUIT per uscire): ");
//                    String matricola = interceptor.nextLine();
//                    if(matricola.equals("QUIT")) {
//                        inserimento_abilitato = false;
//                    }
//                    else {
//                        String studente = nome + " " + cognome + " - #" + matricola;
//                        lista_studenti += studente + "\n";
//
//                        System.out.println("------- LISTA -------");
//                        System.out.println(lista_studenti);
//                    }
//                }
//            }
//        }
//
//        System.out.println("PROGRAMMA TERMINATO");
//
//        interceptor.close();
//    }
//
//}

/*
 * CREARE UN PROGRAMMA CON MENU
 * 1 - Inserisci Studente
 *          - Inserisci il nome
 *          - Inserisci il cognome
 *          - Inserisci la matricola
 * 2 - Ricerca studente
 *          - Inserisci la matricola
 */

    	int scelta, numeroStudenti= 0; 
    	String ListaStudenti = "", studente, nome , cognome, matricola, Intro = "\nGli studenti sono:\n"; 
    	boolean inserimento = true;
    	Scanner interceptor = new Scanner(System.in);
    	
    	do {
    		System.out.println("MENU: \n 1 - Inserisci studente\n 2 - Ricerca studente\n"
    				+ " 3 - Conteggio studenti\n 4 - QUIT\n"
    				+ "\n ---------------- \n");
    		scelta = Integer.parseInt(interceptor.nextLine());
    	switch (scelta) {
    		case 1:
    			System.out.println("Inserire il nome:");
    			nome = interceptor.nextLine();
    			
				System.out.println("Inserire il cognome:");
				cognome = interceptor.nextLine();
				
				System.out.println("Inserire la matricola:");
				matricola = interceptor.nextLine();
				if (ListaStudenti.contains(matricola.trim())) 
					System.out.println("Errore, matricola gi� presente");
				else
				{
					studente = nome + ", " + cognome + ", #" + matricola + "\n"; 
					numeroStudenti++;
					ListaStudenti += studente + "\n";
				}
				
				System.out.println(Intro + ListaStudenti + "\n-----------------------\n");
    			break;
    		case 2: 
    			
    			break;
    		case 3: 
    			System.out.println("Gli studenti nel registro sono:" + numeroStudenti);
    			break;
    		case 4:
    			inserimento = false;
    			break;
    		default:
    			System.out.println("Scelta non valida");
    	}
    			
    	} while (inserimento);
    	
    	interceptor.close();
    	
    	System.out.println("PROGRAMMA TERMINATO");
    }}

//    	int cont= 0;
//    	String[] arrayAuto = {"BMW", "Toyota", "FIAT", "Ferrari", "Maserati"};
//    
//    	while (cont < arrayAuto.length) {
//    		System.out.println(arrayAuto[cont]);
//    		cont++;
//    	}   
//    
//}}
