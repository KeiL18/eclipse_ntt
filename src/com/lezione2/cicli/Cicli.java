package com.lezione2.cicli;

import java.util.Scanner;

public class Cicli {

    public static void main(String[] args) {

//      int val_max = 0;
//      int indice = 0;
//
//      while(indice < val_max) {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;           //indice =  indice + 1; / indice += 1;
//      }

        /* ------------------------ */

//      int val_max = 0;
//      int indice = 0;
//
//      do {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;
//
//      } while (indice < val_max);

        /* ------------------------ */

//      Scanner interceptor = new Scanner(System.in);       //Dichiario lo Scanner
//
//      int val_max = 3;
//      int i=0;
//
//      while(i<val_max) {
//          System.out.println("Dimmi qual'� il tuo nome: ");
//          String nome = interceptor.nextLine();
//          System.out.println("Ciao " + nome);
//
//          i++;
//      }
//
//      interceptor.close();

        /* ------------------------ */

      Scanner interceptor = new Scanner(System.in);

      boolean scrittura_abilitata = true;

      while(scrittura_abilitata) {
          System.out.println("Dimmi qual'� il tuo nome (digita QUIT per uscire): ");
          String nome = interceptor.nextLine();

          if(nome.equals("QUIT")) {
              scrittura_abilitata = false;
          }
          else {
              System.out.println("Ciao " + nome);
          }
      }

      System.out.println("Programma terminato");

      interceptor.close();

     
    }

}
