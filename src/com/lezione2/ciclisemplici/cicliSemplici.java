package com.lezione2.ciclisemplici;

import java.util.Scanner;

public class cicliSemplici {

    public static void main(String[] args) {

//      int val_max = 0;
//      int indice = 0;
//
//      while(indice < val_max) {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;           //indice =  indice + 1; / indice += 1;
//      }

        /* ------------------------ */

//      int val_max = 0;
//      int indice = 0;
//
//      do {
//          System.out.println("Il valore dell'indice �: " + indice);
//
//          indice++;
//
//      } while (indice < val_max);

        /* ------------------------ */

//      Scanner interceptor = new Scanner(System.in);       //Dichiario lo Scanner
//
//      int val_max = 3;
//      int i=0;
//
//      while(i<val_max) {
//          System.out.println("Dimmi qual'� il tuo nome: ");
//          String nome = interceptor.nextLine();
//          System.out.println("Ciao " + nome);
//
//          i++;
//      }
//
//      interceptor.close();

        /* ------------------------ */

//      Scanner interceptor = new Scanner(System.in);
//
//      boolean scrittura_abilitata = true;
//
//      while(scrittura_abilitata) {
//          System.out.println("Dimmi qual'� il tuo nome (digita QUIT per uscire): ");
//          String nome = interceptor.nextLine();
//
//          if(nome.equals("QUIT")) {
//              scrittura_abilitata = false;
//          }
//          else {
//              System.out.println("Ciao " + nome);
//          }
//      }
//
//      System.out.println("Programma terminato");
//
//      interceptor.close();

        /*
         * Creare un sistema di input che abbia come finalit� la creazione di un elenco di studenti.
         * Uno studente � caratterizzato da NOME, COGNOME, MATRICOLA (tutto alfanumerico)
         * Ad ogni inserimento, questi studenti verranno elencati sequenzialmente e stampati in Console.
         *
         * TIP: Create una stringa chiamata Lista Studenti!
         */

//      Scanner interceptor = new Scanner(System.in);
//
//      boolean inserimento_abilitato = true;
//      String lista_studenti = "";
//
//      while(inserimento_abilitato) {
//          System.out.println("Dimmi qual'� il tuo nome (digita QUIT per uscire): ");
//          String nome = interceptor.nextLine();
//          if(nome.equals("QUIT")) {
//              inserimento_abilitato = false;
//          }
//          else {
//              System.out.println("Dimmi qual'� il tuo cognome (digita QUIT per uscire): ");
//              String cognome = interceptor.nextLine();
//              if(cognome.equals("QUIT")) {
//                  inserimento_abilitato = false;
//              }
//              else {
//                  System.out.println("Dimmi qual'� la tua matricola (digita QUIT per uscire): ");
//                  String matricola = interceptor.nextLine();
//                  if(matricola.equals("QUIT")) {
//                      inserimento_abilitato = false;
//                  }
//                  else {
//                      String studente = nome + " " + cognome + " - #" + matricola;
//                      lista_studenti += studente + "\n";
//
//                      System.out.println("------- LISTA -------");
//                      System.out.println(lista_studenti);
//                  }
//              }
//          }
//      }
//
//      System.out.println("PROGRAMMA TERMINATO");
//
//      interceptor.close();

        /*
         * CREARE UN PROGRAMMA CON MENU
         * 1 - Inserisci Studente
         *          -> Inserisci il nome
         *          -> Inserisci il cognome
         *          -> Inserisci la matricola
         *
         * 2 - Ricerca studente
         *          -> Inserisci la matricola
         *          <- Lo studente � presente / non � presente
         *
         * CHALLENGE: Inserisco lo studente se e solo se la matricola � assente nella lista.
         * CHALLENGE: Man mano che inserisco gli studenti, conto quanti ce ne sono in lista!
         * ---- Prevedere una voce del menu che mi restituisca il conto degli studenti.
         */

        Scanner interceptor = new Scanner(System.in);

        boolean scrittura_abilitata = true;
        String lista_studenti = "";
        int numero_studenti = 0;

        while(scrittura_abilitata) {
            System.out.println("Dimmi cosa vuoi fare:\n"
                    + "A - Inserisci uno Studente\n"
                    + "N - Numero Studenti salvati\n"
                    + "R - Ricerca uno Studente\n"
                    + "S - Svuota lista\n"
                    + "Q - QUIT");

            String scelta = interceptor.nextLine();

            switch(scelta) {
                case "A":
                    System.out.println("Dimmi qual'� il tuo nome: ");
                    String nome = interceptor.nextLine();
                    System.out.println("Dimmi qual'� il tuo cognome: ");
                    String cognome = interceptor.nextLine();
                    System.out.println("Dimmi qual'� il tuo matricola: ");
                    String matricola = interceptor.nextLine();

                    if(lista_studenti.indexOf(matricola) != -1) {
                        System.out.println("Errore, matricola gi� presente!");
                    }
                    else {
                        String studente = nome + " " + cognome + " - #" + matricola;
                        lista_studenti += studente;

                        numero_studenti++;      //Incremento del numero di studenti
                    }
                    break;
                case "N":
                    System.out.println("Numero di studenti: " + numero_studenti + "\n");
                    break;
                case "R":
                    System.out.println("Dimmi qual'� il tuo matricola: ");
                    matricola = interceptor.nextLine();

                    if(lista_studenti.indexOf(matricola.trim()) != -1) {
                        System.out.println("Lo studente esiste nella lista!");
                    }
                    else {
                        System.out.println("Lo studente NON esiste nella lista!");
                    }
                    break;
                case "S":
                    lista_studenti = "";
                    break;
                case "Q":
                    scrittura_abilitata = false;
                    break;
                default:
                    System.out.println("Non hai effettuato nessuna scelta!\n");
            }
        }

        System.out.println("PROGRAMMA TERMINATO");

        interceptor.close();
    }

}
