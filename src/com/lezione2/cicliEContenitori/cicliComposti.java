package com.lezione2.cicliEContenitori;

public class cicliComposti {
	

	    public static void main(String[] args) {
	    	
//	      int indice = 0;
//	      int max_val = 5;
//	      while(indice <= max_val) {
//	          System.out.println("Valore indice: " + indice);
	//
//	          indice++;
//	      }

	        /* ------------------------------ */

//	      for(int indice = 0; indice <= 5; indice++) {
//	          System.out.println("Valore indice: " + indice);
//	      }

	        /* CONTENITORI */

//	      int[] array_numeri = { 15, 89, 78, 95, 23 };
//	      System.out.println(array_numeri[2]);

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI"
//	      };
	//
//	      System.out.println(array_automobili[2]);

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI",
//	              "Maserati"
//	      };

	        //System.out.println(array_automobili.length);
//	      System.out.println(array_automobili[0]);
//	      System.out.println(array_automobili[1]);
//	      System.out.println(array_automobili[2]);
//	      System.out.println(array_automobili[3]);

	        //Ciclo FOR
//	      for(int i = 0; i < array_automobili.length; i++) {
//	          System.out.println(array_automobili[i]);
//	      }

//	      System.out.println(array_automobili[array_automobili.length - 1]);  //Ultimo elemento dell'array

	        //Equivalente del for in while
//	      int indice = 0;
//	      while(indice < array_automobili.length) {
//	          System.out.println(array_automobili[indice]);
	//
//	          indice++;
//	      }

	        /* -------------------------------------- */

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI",
//	              "Maserati"
//	      };
	//
//	      //Equivalente del FOR in FOREACH
//	      for(String auto: array_automobili) {
//	          System.out.println(auto);
//	      }

	        /* ------------------------------- */

//	      int[] array_numeri = { 15, 89, 78, 95, 23 };

//	      for(int i=0; i < array_numeri.length; i++) {
//	          System.out.println("Il valore all posizione " + i + " �: " + array_numeri[i]);
//	      }

	        //System.out.println(i); I non esiste!

	        /* ------------------------------- */

//	      int[] array_numeri = { 15, 89, 78, 95, 23 };
//	      int indice = 0;
	//
//	      while(indice < array_numeri.length) {
//	          System.out.println(array_numeri[indice]);
//	          indice++;
//	      }
	//
//	      System.out.println("---->" + indice);
	//
//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI",
//	              "Maserati"
//	      };
	//
//	      indice = 0;
//	      while(indice < array_automobili.length) {
//	          System.out.println(array_automobili[indice]);
//	          indice++;
//	      }

	        /*
	         * All'interno dell'array_automobili cercare la marca FIAT e se c'� dirmi anche la posizione!
	         */

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI",
//	              "Maserati"
//	      };
	//
//	      String ricerca = "FIAT";


//	      boolean trovato = false;                            //O(1)
//	      for(int i=0; i<array_automobili.length; i++) {      //*n
	//
//	          if(ricerca.equals(array_automobili[i])) {       //O(1)
//	              System.out.println("Trovato alla posizione: " + i); //O(1) - opz
//	              trovato = true;                             //O(1) - ops
//	          }
//	      }
	//
//	      if(!trovato) {                                      //O(1)
//	          System.out.println("Non l'ho trovata!");        //O(1)
//	      }

	        //Soluzioni equivalenti.
//	      int posizione_trovato = -1;                         //O(1)
//	      for(int i=0; i<array_automobili.length; i++) {      //*n
//	          if(ricerca.equals(array_automobili[i])) {       //O(1)
//	              posizione_trovato = i;                      //O(1) - opz
//	          }
//	      }
	//
//	      if(posizione_trovato != -1)                                                 //O(1)
//	          System.out.println("Trovato nella posizione: " + posizione_trovato);    //O(1)
//	      else
//	          System.out.println("Non l'ho trovata!");                                //O(1)

	        /* ----------------------------------------- */

//	      String[][] lista_studenti = {
//	              {"Giovanni", "Pace", "#123ABC"},
//	              {"Mario", "Rossi", "#321BCA"},
//	              {"Valeria", "Verdi"},
//	              {"Maria", "Viola", "#543ERT", "123456789"}
//	      };
	//
////	        System.out.println(lista_studenti[1]);
	//
//	      for(int k=0; k<lista_studenti.length; k++) {                //Per ogni riga
	//
//	          for(int i=0; i<lista_studenti[k].length; i++) {         //Per ogni colonna
//	              System.out.println(lista_studenti[k][i]);           //Output
//	          }
	//
//	          System.out.println("-------------------");
//	      }

	        /* ----------------------------------------- */

//	      String[][] lista_studenti = {
//	              {"Giovanni", "Pace", "#123ABC"},
//	              {"Mario", "Rossi", "#321BCA"},
//	              {"Valeria", "Verdi"},
//	              {"Maria", "Viola", "#543ERT", "123456789"}
//	      };
	//
////	        lista_studenti[1][3] = "QAABIEFOIGOIHGOK";                  //Non funziona, out of boud!
	//
//	      for(int k=0; k<lista_studenti.length; k++) {                //Per ogni riga
	//
//	          String studente = "";
	//
//	          for(int i=0; i<lista_studenti[k].length; i++) {         //Per ogni colonna
//	              studente += lista_studenti[k][i] + " ";         //Output
//	          }
	//
//	          System.out.println(studente + "\n-------------------");
//	      }






	        String[][] lista_studenti = {
	                {"Giovanni", "Pace", "#123ABC"},
	                {"Mario", "Rossi", "#321BCA"},
	                {"Valeria", "Verdi"},
	                {"Maria", "Viola", "#543ERT", "123456789"}
	        };

	        /*
	         * Voglio in output tutti i nomi degli studenti!
	         * Voglio che mi cerchiate un singolo studente per Matricola!
	         */



	    }}



//	      int indice = 0;
//	      int max_val = 5;
//	      while(indice <= max_val) {
//	          System.out.println("Valore indice: " + indice);
	//
//	          indice++;
//	      }

	        /* ------------------------------ */

//	      for(int indice = 0; indice <= 5; indice++) {
//	          System.out.println("Valore indice: " + indice);
//	      }

	        /* CONTENITORI */

//	      int[] array_numeri = { 15, 89, 78, 95, 23 };
//	      System.out.println(array_numeri[2]);

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI"
//	      };
	//
//	      System.out.println(array_automobili[2]);

//	      String[] array_automobili = {
//	              "BMW",
//	              "Toyota",
//	              "FIAT",
//	              "FERRARI",
//	              "Maserati"
//	      };

	        //System.out.println(array_automobili.length);
//	      System.out.println(array_automobili[0]);
//	      System.out.println(array_automobili[1]);
//	      System.out.println(array_automobili[2]);
//	      System.out.println(array_automobili[3]);

	        //Ciclo FOR
//	      for(int i = 0; i < array_automobili.length; i++) {
//	          System.out.println(array_automobili[i]);
//	      }

//	      System.out.println(array_automobili[array_automobili.length - 1]);  //Ultimo elemento dell'array

	        //Equivalente del for in while
//	      int indice = 0;
//	      while(indice < array_automobili.length) {
//	          System.out.println(array_automobili[indice]);
	//
//	          indice++;
//	      }

	        /* -------------------------------------- */

//	        String[] array_automobili = {
//	                "BMW",
//	                "Toyota",
//	                "FIAT",
//	                "FERRARI",
//	                "Maserati"
//	        };
//
//	        //Equivalente del FOR in FOREACH
//	        for(String auto: array_automobili) {
//	            System.out.println(auto);
//	        }
//
//	    }

//	    int[] array_numeri = { 15, 89, 78, 95, 23 };
//	    int totale = 0;
//	    for (int i=0; i < array_numeri.length; i++) 
//	    {
//	    	totale += array_numeri[i];
//	    }
	    	
//	    	String[] array_automobili = {
//	                "BMW",
//	                "Toyota",
//	                "FIAT",
//	                "FERRARI",
//	                "Maserati"
//	        };
//	    
//	    	int mem = -1;
//	    	for (int i = 0; i < array_automobili.length; i++)
//	    	{
//	    		if (array_automobili[i].equals("FIAT")){
//	    			mem = i;
//	    		}
//	    	}
//	    	if (mem != -1)
//	    		System.out.println("FIAT � presente nella posizione: " + mem);
//	    	else
//	    		System.out.println("FIAT non � presente");
//	    }
//}
