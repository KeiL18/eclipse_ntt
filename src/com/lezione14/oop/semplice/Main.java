package com.lezione14.oop.semplice;

public class Main {
	
	private static final SESSO Maschio = null;

	public static void main(String [] args) {
		
		//creo le mie effettive lattine specifiche	
//      Libro libro_1 = new Libro();
//      libro_1.titolo = "Il signore degli anelli";
//      libro_1.autore = "Tolkien";
//      libro_1.isbn = "123456-123456-123456";
//      libro_1.anno_pubblicazione = 1920;
//
//      Libro libro_2 = new Libro();
//      libro_2.titolo = "Guerra e Pace";
//      libro_2.autore = "Tolstoj";
//
//      System.out.println(libro_1.titolo);
//      System.out.println(libro_2.titolo);

        Libro libro_3 = new Libro();
        libro_3.setTitolo("Il signore degli anelli");
        libro_3.setAutore("Tolkien");
        libro_3.setISBN("123456-123456-123456");
        libro_3.setAnnoPubblicazione(1920);
//      libro_3.stampaLibro();

        Libro libro_4 = new Libro();
        libro_4.setTitolo("Il ritorno del re");
        libro_4.setAutore("Tolkien");
        libro_4.setISBN("987654-123456-123456");
        libro_4.setAnnoPubblicazione(1924);
        libro_4.setDedica("A mia moglie");
//      libro_4.stampaLibro();

        libro_3.stampaLibroEstesa();
        libro_4.stampaLibroEstesa();
        
        Studente giovanni = new Studente();
        giovanni.setNome("Giovanni");
        giovanni.setCognome("Pace");
        giovanni.setMatricola("123456");
        giovanni.setSesso(Maschio);
        giovanni.stampaStudente();

//        Studente mario = new Studente("Mario", "Rossi", "maschio");
//        mario.stampaStudente();
//        mario.setMatricola("789456");
//        mario.stampaStudente();

//        Studente valeria = new Studente("Valeria", "Viola", "123456456789");
//        valeria.stampaStudente();
    }

}
