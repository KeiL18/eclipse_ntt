package com.lezione14.oop.semplice;

enum SESSO{
	maschio,
	femmina,
	altro
}

public class Studente {

	//HandsOn: creazione di una classe per gli studenti come fatto per i form in HTML e JS
	private String nome;
	private String cognome;
	private String matricola;
	private SESSO sesso;
	
	Studente (){
		
	}
	
	Studente (String var_nome, String var_cognome, SESSO var_sesso){
		this.nome = var_nome;
		this.cognome = var_cognome;
		this.matricola = "Non definita!";
		this.sesso = var_sesso ;
	}
	
	public void setNome(String var_nome) {
		if (var_nome.trim().isEmpty()) {
			System.out.println("[ERR] Nome non inserito!");
		}
		else {
			this.nome = var_nome;
		}
	}
		
	public void setCognome(String var_cognome) {
		if (var_cognome.trim().isEmpty()) {
			System.out.println("[ERR] Cognome non inserito!");
		}
		else {
			this.cognome = var_cognome;
		}
	}
	
	public void setMatricola(String var_matricola) {
		if (var_matricola.trim().isEmpty()) {
			System.out.println("[ERR] Matricola non inserita!");
		}
		else {
			this.matricola = formattaMatricola(var_matricola);
		}
	}
	
	public void setSesso(SESSO var_sesso) {
		this.sesso = var_sesso;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getCognome() {
		return this.cognome;
	}
	
	public String getMatricola() {
		return this.matricola;
	}
	
	public SESSO getSesso() {
		return this.sesso;
	}
	
	public void stampaStudente() {
		System.out.println(
                this.nome + ", " +
                this.cognome + ", " +
                this.matricola + ", " +
                this.sesso
                );
	}
	
	public void stampaStudenteEstesa() {
        System.out.println("Nome: " + this.nome);
        System.out.println("Cognome: " + this.cognome);
        System.out.println("Matricola: " + this.matricola);
        System.out.println("Sesso: " + this.sesso);
        System.out.println("--------------");
    }
	
	private String formattaMatricola(String var_matricola) {
		return "2021-"+ var_matricola;
	}
}
