package com.lezione14.oop.semplice;

public class Libro {
	//definisco le caratteristiche generali della classe [lattina]
	private String titolo;
    private String autore;
    private String isbn;
    private int anno_pubblicazione;
    private String dedica = "N.D.";

    public void setTitolo(String var_titolo) {
        if(var_titolo.isEmpty()) {
            this.titolo = "NON DEFINITO";
        }
        else {
            this.titolo = var_titolo;
        }
    }
    public String getTitolo() {
        return this.titolo;
    }

    public void setAutore(String var_autore) {
        this.autore = var_autore;
    }
    public String getAutore() {
        return this.autore;
    }

    public void setISBN(String var_isbn) {
        this.isbn = var_isbn;
    }
    public String getISBN() {
        return this.isbn;
    }

    public void setAnnoPubblicazione(int var_anno) {
        this.anno_pubblicazione = var_anno;
    }
    public int getAnnoPubblicazione() {
        return this.anno_pubblicazione;
    }

    public void setDedica(String var_dedica) {
        this.dedica = var_dedica;
    }
    public String getDedica() {
        return this.dedica;
    }

    public void stampaLibro() {
        System.out.println(
                this.autore + ", " +
                this.titolo + ", " +
                this.isbn + ", " +
                this.anno_pubblicazione + ", " +
                this.dedica
                );
    }

    public void stampaLibroEstesa() {
        System.out.println("Autore: " + this.autore);
        System.out.println("Titolo: " + this.titolo);
        System.out.println("ISBN: " + this.isbn);
        System.out.println("Anno: " + this.anno_pubblicazione);
        System.out.println("Dedica: " + this.dedica);
        System.out.println("--------------");
    }
}
