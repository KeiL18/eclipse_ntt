package com.lezione18.oop.esercizio_carte_banca;

public class CartaDebito extends Carta{

	private float deposito;

	public float getDeposito() {
		return deposito;
	}

	public void setDeposito(float deposito) {
		this.deposito = deposito;
	}

	public String stampaCarta() {
		return " Nome del titolare:" + nom_titolare + "\n Cognome del titolare:" + cog_titolare +
				"\n Numero della carta: " + num_carta + "\n Deposito:" + deposito;
	}
	
	CartaDebito(String var_NT, String var_CT, float var_depo) {
		this.setNom_titolare(var_NT);
		this.setCog_titolare(var_CT);
		this.setNum_carta();
		this.setDeposito(var_depo);
}
}
