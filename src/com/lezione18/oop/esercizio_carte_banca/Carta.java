package com.lezione18.oop.esercizio_carte_banca;

public class Carta {

	protected static int contatore;
	protected String nom_titolare;
	protected String cog_titolare;
	protected String num_carta;
	
	public String getNom_titolare() {
		return nom_titolare;
	}
	public void setNom_titolare(String nom_titolare) {
		this.nom_titolare = nom_titolare;
	}
	public String getCog_titolare() {
		return cog_titolare;
	}
	public void setCog_titolare(String cog_titolare) {
		this.cog_titolare = cog_titolare;
	}
	public String getNum_carta() {
		return num_carta;
	}
	public void setNum_carta() {
		this.num_carta = String.valueOf((contatore++) + 987500000).toString();
	}
	
	public String stampaCarta() {
		return "";
	}
}
