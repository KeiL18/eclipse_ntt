package com.lezione18.oop.esercizio_carte_banca;

public class CartaCredito extends Carta{

	private float fido;

	public float getFido() {
		return fido;
	}

	public void setFido(float fido) {
		this.fido = fido;
	}

	public String stampaCarta() {
		return " Nome del titolare: " + nom_titolare + "\n Cognome del titolare: " + cog_titolare +
				"\n Numero della carta: " + num_carta + "\n Fido: " + fido;
	}
	
	CartaCredito(String var_NT, String var_CT, float var_fido) {
			this.setNom_titolare(var_NT);
			this.setCog_titolare(var_CT);
			this.setNum_carta();
			this.setFido(var_fido);
	}
}
