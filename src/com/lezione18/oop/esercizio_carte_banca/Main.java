package com.lezione18.oop.esercizio_carte_banca;

public class Main {

	public static void main(String[] args) {
		
		/*
		* Creare un sistema di gestione carte all'interno di una banca
		* Le carte possono differenziarsi in:
		* - Carta di Credito
		* - Carta di Debito
		*
		* La Carta di Credito ha un attributo personalizzato che si chiama FIDO
		* La Carta di Debito ha un attributo personalizzato che si chiama DEPOSITO
		*
		* Voglio sapere, quante carte di credito e di debito ho nella mia banca SENZA
		* utilizzare metodi statici!
		*/
		
		Banca SanPaolo = new Banca();
		SanPaolo.creaCarta("credito", "Andrea", "Luca", 55.2f);
		SanPaolo.creaCarta("credito", "Giovanni", "Pace", 12355.2f);
		SanPaolo.creaCarta("Debito", "Andrea", "Luca", 0f);
		SanPaolo.creaCarta("Debito", "Pippo", "Baudo", -53f);
		SanPaolo.creaCarta("credito", "Frank", "Davis", -1532.25f);
		SanPaolo.creaCarta("credito", "Carlo", "Bianchi", 124555.2f);

		SanPaolo.stampaMemoria();
		
		SanPaolo.contaCarte();
	}

}
