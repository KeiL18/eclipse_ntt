package com.lezione18.oop.esercizio_carte_banca;

import java.util.ArrayList;

public class Banca {
	
	private ArrayList<Carta> array_carte = new ArrayList<Carta>();
	
	/**
	 * Funzione per la generazione di una carta	
	 * @param var_tipo Da scegliere tra credito o debito
	 * @param var_NT Nome del titolare
	 * @param var_CT Cognome del titolare
	 * @param var_saldo Saldo della carta
	 */
	public void creaCarta(String var_tipo, String var_NT, String var_CT, float var_saldo){
		var_tipo = var_tipo.toUpperCase();
		switch (var_tipo) {
			case "CREDITO":
				CartaCredito temp_cre = new CartaCredito(var_NT, var_CT, var_saldo);
				array_carte.add(temp_cre);
				break;
			case "DEBITO":
				if( var_saldo >= 0 ) {
					CartaDebito temp_deb = new CartaDebito(var_NT, var_CT, var_saldo);
					array_carte.add(temp_deb);
				}
				else {
					System.out.println("[ERR] Carta non creata");
				}
				break;
			default:
				System.out.println("[ERR] Carta non creata");
		}
	}
	
	/**
	 * Funzione per la stampa di tutte le carte presenti in memoria
	 */
	public void stampaMemoria() {
		for (int i = 0; i < array_carte.size(); i++) {
			Carta temp = array_carte.get(i);
			System.out.println(temp.stampaCarta() + 
					"\n -------------------------");
		}
	}
	
	/**
	 * Funzione per il conteggio delle carte per tipologia.
	 */
	public void contaCarte() {
		int CD = 0, CC = 0;
		for (int i = 0; i < array_carte.size(); i++) {
			Carta temp = new Carta();
			temp= array_carte.get(i);
			if(temp instanceof CartaCredito) {
				CC++;
			}
			else {
				CD++;
			}
		}
		System.out.println("Ci sono " + CC + " carte di credito e " + CD + " carte di debito");
	}
}
