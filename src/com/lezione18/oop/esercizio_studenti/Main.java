package com.lezione18.oop.esercizio_studenti;

public class Main {

	public static void main(String[] args) {
		
		/*
		* Creare una struttura di immagazinamento di un elenco Studenti
		* cercando di nascondere la struttura di immagazzinamento dati.
		*
		* Voglio in particolare che ci sia una funzione dedicata a:
		* - inserimento
		* - ricerca
		* - eliminazione (per matricola)
		* - stampa dell'elenco
		*
		*/

		GestioneStudenti gestione = new GestioneStudenti();
		gestione.inserimentoStudente("Giovanni", "Pace", "1986-01-01", "123456", 'M');
		gestione.inserimentoStudente("Mario", "Rossi", "2012-12-11", "987654654", 'M');
		gestione.inserimentoStudente("Valeria", "Verdi", "1987-04-25", "8789797978", 'F');
		
		//In un futuro non troppo remoto...
//		gestore.ricercaStudente("987654654");
		
		gestione.ricercaStudente("");
		
//		if(gestore.eliminaStudente("987654654")) {
//			System.out.println("Eliminazione effettuata con successo!");
//		}
//		else {
//			System.out.println("Errore di eliminazione!");
//		}
//		
//		gestore.ricercaStudente("");
		
		if(gestione.modificaStudente("Giggino", "Giggetto", "987654654")) {
			System.out.println("Modifica effettuata con successo!");
		}
		else {
			System.out.println("Errore di modifica!");
		}
		
		gestione.ricercaStudente("");
	}

}
