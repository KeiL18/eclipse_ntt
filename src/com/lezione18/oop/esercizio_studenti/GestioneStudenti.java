package com.lezione18.oop.esercizio_studenti;

import java.util.ArrayList;

public class GestioneStudenti {

	//VERSIONE DIPENDENTE DALLA MATRICOLA
	
	private ArrayList<Studente> elenco_studenti = new ArrayList<Studente>();
	
	GestioneStudenti(){
		System.out.println("Ho appena creato il middleware di gestione studenti");
	}
	
	/**
	 * Funzione di inserimento di uno studente in un elenco
	 * @param var_nom Nome dello studente
	 * @param var_cog Cognome dello studente
	 * @param var_dat Data di nascita dello studente
	 * @param var_mat Matricola dello studente
	 * @param var_ses Sesso dello studente
	 */
	public void inserimentoStudente(String var_nom, String var_cog, String var_dat, String var_mat, char var_ses) {
		Studente stud_temp = new Studente(var_nom, var_cog, var_dat, var_mat,var_ses);
		
		elenco_studenti.add(stud_temp);
	}
	
	public void ricercaStudente(String var_mat) {
		for (int i = 0; i < elenco_studenti.size(); i++) {
			Studente temp = elenco_studenti.get(i);
			if((temp.getMatricola()).equals(var_mat)) {
				System.out.println(temp.dettaglioStudente());
			}
		}
			
	}
	
	public boolean eliminaStudente(String var_matr) {
		boolean esito_eliminazione = false;
		
		if(var_matr.isEmpty()) {
			return esito_eliminazione;
		}
		
		for(int i=0; i<elenco_studenti.size(); i++) {
			
			Studente temp = elenco_studenti.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				elenco_studenti.remove(i);
				esito_eliminazione = true;
			}
			
		}
		return esito_eliminazione;
	}
	
	public boolean modificaStudente(String var_nome, String var_cogn, String var_matr) {
		boolean esito_aggiornamento = false;
		
		if(var_matr.isEmpty()) {
			return esito_aggiornamento;
		}
		
		for(int i=0; i<elenco_studenti.size(); i++) {
			Studente temp = elenco_studenti.get(i);
			if(temp.getMatricola().equals(var_matr)) {
				temp.setNome(var_nome);
				temp.setCognome(var_cogn);
				
				esito_aggiornamento = true;
			}
		}
		
		return esito_aggiornamento;
	}
	
}
