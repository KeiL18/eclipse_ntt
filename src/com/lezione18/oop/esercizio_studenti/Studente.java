package com.lezione18.oop.esercizio_studenti;

public class Studente extends GestioneStudenti{

	//VERSIONE DIPENDENTE DALLA MATRICOLA
	
	private String nome;
	private String cognome;
	private String data_nasc;
	private String matricola;
	private char sesso;
	
	Studente(String var_nom, String var_cog, String var_dat, String var_mat, char var_ses){
		this.nome = var_nom;
		this.cognome = var_cog;
		this.data_nasc = var_dat;
		this.matricola = var_mat;
		this.sesso = var_ses;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getData_nasc() {
		return data_nasc;
	}
	public void setData_nasc(String data_nasc) {
		this.data_nasc = data_nasc;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public char getSesso() {
		return sesso;
	}
	public void setSesso(char sesso) {
		this.sesso = sesso;
	}
	
	public String dettaglioStudente() {
		return "Nome: " + this.getNome() + "\n"
				+ "Cognome: " + this.getCognome() + "\n"
				+ "Matricola: " + this.getMatricola() + "\n"
				+ "Data di nascita: " + this.getData_nasc() + "\n"
				+ "Sesso: " + this.getSesso() + "\n"
				+ "-----------------------------";
	}
}
