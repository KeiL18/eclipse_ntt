package com.lezione18.oop.esercizio_biblioteca;

public class Libro extends Supporto{
	
	private String autore;
	private String isbn;
	
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	@Override
	public String stampaDettagli() {
			return "Titolo: " + titolo + "\n"
					+ "Autore: " + autore + "\n"
					+ "Isbn: " + isbn + "\n"
					+ "Scaffale: " + scaffale + "\n"
					+ "Posizione: " + posizione + "\n"
					+ "Matricola: " + matricola + "\n";
		}
}
	
