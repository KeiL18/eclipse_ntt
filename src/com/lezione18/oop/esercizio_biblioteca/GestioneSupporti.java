package com.lezione18.oop.esercizio_biblioteca;

import java.util.ArrayList;

public class GestioneSupporti {

	private ArrayList<Supporto> elenco_supporti = new ArrayList<Supporto>();
	
	public void aggiungiSupporto(String var_tipo, String var_scaf, String var_pos, String var_tit, String var_aut, String var_num) {
		
		switch(var_tipo) {
			case "Libro":
				Libro lib_temp = new Libro();
				lib_temp.setAutore(var_aut);
				lib_temp.setPosizione(var_pos);
				lib_temp.setScaffale(var_scaf);
				lib_temp.setTitolo(var_tit);
				lib_temp.setIsbn(var_num);
				lib_temp.setMatricola();
				
				elenco_supporti.add(lib_temp);
				break;
			case "Rivista":
				Rivista riv_temp = new Rivista();
				riv_temp.setPosizione(var_pos);
				riv_temp.setScaffale(var_scaf);
				riv_temp.setTitolo(var_tit);
				riv_temp.setNumero(var_num);
				riv_temp.setMatricola();
				
				elenco_supporti.add(riv_temp);
				break;
			default:
				System.out.println("[ERR] Tipologia inesistente!");
		}
	}
	
	public void stampaSupporti () {
		for (int i = 0; i < elenco_supporti.size(); i++) {
			Supporto temp = elenco_supporti.get(i);
			System.out.println(temp.stampaDettagli());

		}
	}
	
	public void ricercaSupporto (String var_tit) {
		for (int i = 0; i < elenco_supporti.size(); i++) {
			Supporto temp = elenco_supporti.get(i);
			if (temp.getTitolo().equals(var_tit)) {
				System.out.println(temp.stampaDettagli());
			}
		}
	}
	
	public void eliminaSupporto (String var_mat) {
		for (int i = 0; i < elenco_supporti.size(); i++) {
			Supporto temp = elenco_supporti.get(i);
			if (temp.getMatricola().equals(var_mat)) {
				elenco_supporti.remove(i);
			}
		}
	}
	
	public void modificaSupporto (String var_mat, String var_scaf, String var_pos, String var_tit, String var_aut, String var_num) {
		for (int i = 0; i < elenco_supporti.size(); i++) {
			Supporto temp = elenco_supporti.get(i);
			if (temp.getMatricola().equals(var_mat)) {
				//TODO
			}
		}
	}
}
