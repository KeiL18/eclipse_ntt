package com.lezione18.oop.esercizio_biblioteca;

public abstract class Supporto {

	protected static int contatore_supporti = 0;
	protected String scaffale;
	protected String posizione;
	protected String titolo;
	protected String matricola;
	
	public String getScaffale() {
		return scaffale;
	}
	public void setScaffale(String scaffale) {
		this.scaffale = scaffale;
	}
	public String getPosizione() {
		return posizione;
	}
	public void setPosizione(String posizione) {
		this.posizione = posizione;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	
	public void setMatricola() {
		contatore_supporti++;
		this.matricola = "SUP-" + (contatore_supporti);
	}
	public String getMatricola() {
		return matricola;
	}
	
	public String stampaDettagli() {
		return "";
	}
}
