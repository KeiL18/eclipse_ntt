package com.lezione18.oop.esercizio_biblioteca;

public class Rivista extends Supporto{
	
	private String numero;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@Override
	public String stampaDettagli() {
		return "Titolo: " + titolo + "\n"
				+ "Numero: " + numero + "\n"
				+ "Scaffale: " + scaffale + "\n"
				+ "Posizione: " + posizione + "\n"
				+ "Matricola: " + matricola + "\n";
	}
	
}
