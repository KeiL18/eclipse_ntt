package com.lezione18.oop.esercizio_biblioteca;

public class Main {

	public static void main(String[] args) {

		/*
		* Scrivere un programma che gestisca il contenuto di una biblioteca.
		* Il contenuto � un Supporto che pu� essere catalogato come "Libro" o "Rivista"
		* Ogni supporto avr� una matricola univoca gestita autonomamente dal sistema!!!!!!!!!!!!!!!!!!!!
		*
		* Permettere le CRUD di Libro o Rivista
		*/

		GestioneSupporti gestione = new GestioneSupporti();
		gestione.aggiungiSupporto("Libro", "55", "A2", "Il mondo nuovo", "Aldus Huxley", "56766774536");
		gestione.aggiungiSupporto("Libro", "55", "B7", "Electric Dreams", "Philip K. Dick", "56766664536");
		gestione.aggiungiSupporto("Rivista", "21B", "AB3", "Motosprint", "", "12/2020");
		gestione.aggiungiSupporto("Rivista", "1F", "AZ2", "Focus", "" , "33/2021");
		gestione.aggiungiSupporto("Libro", "18", "Z2", "BroCode", "Barney Stinson" , "92874848");
		
		gestione.stampaSupporti();
		
		gestione.ricercaSupporto("Electric Dreams");
		
		gestione.eliminaSupporto("SUP-4");
		
		gestione.stampaSupporti();
	}

}
