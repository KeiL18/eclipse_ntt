package com.lezione18.oop.esercizio_carte_banca_soluzione;

public class Debito extends SupportoMagnetico{

	private float deposito = 0;

	Debito(String var_codice, String var_intestatario, float var_deposito){
		super.codice = var_codice;
		super.intestatario = var_intestatario;
		this.deposito = var_deposito;
	}
	
	public float getDeposito() {
		return deposito;
	}

	public void setDeposito(float deposito) {
		this.deposito = deposito;
	}
}
