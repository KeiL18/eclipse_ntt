package com.lezione18.oop.esercizio_carte_banca_soluzione;

public class Main {

	public static void main(String[] args) {

		IstitutoBancario centrale = new IstitutoBancario("Banca Centrale");
	
		centrale.insert("CREDITO", "123456", "PACE GIOVANNI", 3000);
		centrale.insert("CREDITO", "123457", "MARIO ROSSI", 3000);
		centrale.insert("CREDITO", "123458", "VALERIA VERDI", 3000);
		centrale.insert("DEBITO", "123459", "VALERIA VERDI", 256.45f);
		centrale.insert("DEBITO", "123459", "MARIO ROSSI", 656.25f);

		System.out.println("CREDITO:" + centrale.contaCarte("CREDITO"));
		System.out.println("DEBITO:" + centrale.contaCarte("DEBITO"));
		System.out.println("TOTALE:" + centrale.contaCarte(""));
		
	}
}
