package com.lezione18.oop.esercizio_carte_banca_soluzione;

public class Credito extends SupportoMagnetico {
	
	private float fido = 0;

	Credito(String var_codice, String var_intestatario, float var_fido){
		super.codice = var_codice;
		super.intestatario = var_intestatario;
		this.fido = var_fido;
	}
	
	public float getFido() {
		return fido;
	}

	public void setFido(float fido) {
		this.fido = fido;
	}
}
