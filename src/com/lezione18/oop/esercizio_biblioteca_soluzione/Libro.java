package com.lezione18.oop.esercizio_biblioteca_soluzione;

public class Libro extends Supporto{
	
	private String autore;
	
	Libro(String var_isbn, String var_titolo, String var_editore, String var_autore){
		super.isbn = var_isbn;
		super.titolo = var_titolo;
		super.editore = var_editore;
		this.autore = var_autore;
		
		super.matricola = "SUP-" + (Supporto.contatore_libri + 1);
		Supporto.contatore_libri++;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	@Override
	public String toString() {
		return "Libro [autore=" + autore + ", isbn=" + isbn + ", titolo=" + titolo + ", editore=" + editore
				+ ", matricola=" + matricola + "]";
	}

}
