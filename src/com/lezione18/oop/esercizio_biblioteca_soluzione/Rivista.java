package com.lezione18.oop.esercizio_biblioteca_soluzione;

public class Rivista extends Supporto{

	private int numero;
	
	Rivista(String var_isbn, String var_titolo, String var_editore, int var_numero){
		super.isbn = var_isbn;
		super.titolo = var_titolo;
		super.editore = var_editore;
		this.numero = var_numero;
		
		super.matricola = "SUP-" + (Supporto.contatore_libri + 1);
		Supporto.contatore_libri++;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Rivista [numero=" + numero + ", isbn=" + isbn + ", titolo=" + titolo + ", editore=" + editore
				+ ", matricola=" + matricola + "]";
	}
}
