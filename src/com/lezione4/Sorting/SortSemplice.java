package com.lezione4.Sorting;

import java.util.Arrays;
import java.util.Collections;

public class SortSemplice {

    public static final int DIMENSIONE = 10;

    public static void main(String[] args) {

//      int[] array_numeri = new int[DIMENSIONE];
//      array_numeri[0] = 34;
//      array_numeri[1] = 87;
//      array_numeri[2] = 2;
//      array_numeri[3] = 76;
//      array_numeri[4] = 90;
//      array_numeri[5] = 4;
//      array_numeri[6] = 5;
//      array_numeri[7] = 12;
//      array_numeri[8] = 65;
//      array_numeri[9] = 98;
//
//      System.out.println(array_numeri);

//      System.out.println(Arrays.toString(array_numeri));

//      Arrays.sort(array_numeri);

//      System.out.println(Arrays.toString(array_numeri));

//      //Ordina l'array al contrario (reverse) senza l'utilizzo di librerie
//      int[] array_reverse = new int[array_numeri.length];
//
////        for(int i=0; i<array_numeri.length; i++) {
////            System.out.println(array_numeri[(array_numeri.length - 1) - i]);
////        }
//
//      for(int i=(array_numeri.length - 1); i>=0; i--) {
//          array_reverse[(array_numeri.length - 1) - i] = array_numeri[i];
//      }
//
//      System.out.println(Arrays.toString(array_numeri));
//      System.out.println(Arrays.toString(array_reverse));

        //INTERI COME TIPO COMPLESSO
//      Integer[] array_sorgente = new Integer[DIMENSIONE];
//      array_sorgente[0] = 34;
//      array_sorgente[1] = 87;
//      array_sorgente[2] = 2;
//      array_sorgente[3] = 76;
//      array_sorgente[4] = 90;
//      array_sorgente[5] = 4;
//      array_sorgente[6] = 5;
//      array_sorgente[7] = 12;
//      array_sorgente[8] = 65;
//      array_sorgente[9] = 98;
//
//      Arrays.sort(array_sorgente);
//      System.out.println(Arrays.toString(array_sorgente));
//
//      Arrays.sort(array_sorgente, Collections.reverseOrder());
//      System.out.println(Arrays.toString(array_sorgente));

        //ALTRE TIPOLOGIE DI DATO
        String[] rubrica = {"Giovanni", "Maria", "Pietro", "Riccardo", "Alessandra", "Libera"};

        System.out.println("UNSORTED");
        System.out.println(Arrays.toString(rubrica));

        System.out.println("SORTED ASC");
        Arrays.sort(rubrica);
        System.out.println(Arrays.toString(rubrica));

        System.out.println("SORTED DESC");
        Arrays.sort(rubrica, Collections.reverseOrder());
        System.out.println(Arrays.toString(rubrica));

    }

}
