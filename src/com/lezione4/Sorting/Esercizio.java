package com.lezione4.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Esercizio {
	
    /*
     * Scrivere un sistema che:
     * - Alla selezione dell'opzione INSERISCI, dato in input un nome lo aggiunga alla rubrica.
     * - Alla selezione dell'opzione STAMPA, venga richiesto se stampare la rubrica in maniera ASC o DESC ed effettua il relativo Output.
     */

	public static void main(String[] args) {
		ArrayList<String> rubrica = new ArrayList<String>();
		Scanner interceptor = new Scanner(System.in);
		boolean inserimento = true, errore = true;
		
		while (inserimento) {
			System.out.println("---------- NUOVA RICHIESTA ----------");
			System.out.println("Scegliere che operazione eseguire:\n I - Inserire un nuovo nome\n S - Stampare la rubrica\n Q - Uscire");
			String scelta1 = interceptor.nextLine();
		
			switch(scelta1) {
				case "I":
					System.out.println("Inserire il nome da aggiungere:");
					String nome = interceptor.nextLine();
					rubrica.add(nome);
					break;
				case "S":
					while (errore) {
						System.out.println("Scegliere l'ordinamento:\n A - Ascendente\n D - Discendente");
						String scelta2 = interceptor.nextLine();
						switch (scelta2) {
							case "A":
								Collections.sort(rubrica);
								System.out.println("La rubrica ordinata �:" + rubrica);
								errore = false;
								break;
							case "D":
								Collections.sort(rubrica, Collections.reverseOrder());
								System.out.println("La rubrica ordinata �:" + rubrica);
								errore = false;
								break;
							default:
								System.out.println("[ERR] Inserimento non valido.");
						}
					}
					
					errore = true;
					break;
				case "Q":
					inserimento = false;
					break;
				default:
					System.out.println("[ERR] Inserimento non valido.");
					
		}
	
		}
		
		interceptor.close();
	}

}
