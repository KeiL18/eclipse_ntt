package com.lezione4.enumeratori;

import java.util.Arrays;

public class SortSemplice {

    public static final int DIMENSIONE = 10;

    public static void main(String[] args) {

        int[] array_numeri = new int[DIMENSIONE];
        array_numeri[0] = 34;
        array_numeri[1] = 87;
        array_numeri[2] = 2;
        array_numeri[3] = 76;
        array_numeri[4] = 90;
        array_numeri[5] = 4;
        array_numeri[6] = 5;
        array_numeri[7] = 12;
        array_numeri[8] = 65;
        array_numeri[9] = 98;

        System.out.println(Arrays.toString(array_numeri));

        Arrays.sort(array_numeri);

        System.out.println(Arrays.toString(array_numeri));
        
        // ordina l'array al contrario (reverse) senza l'utilizzo di librerie
        
        int [] array_numeri_Inverso = new int [DIMENSIONE];
        
        invertiArray(array_numeri, array_numeri_Inverso);
        
        System.out.println(Arrays.toString(array_numeri_Inverso));
    }

    public static void invertiArray (int[] ingresso, int[] uscita) {
    	
    	for (int i = 0; i < ingresso.length; i++) {
    		uscita[DIMENSIONE-1-i] = ingresso[i];
    	}
    }
}
