package com.lezione3.metodi;

import java.util.Scanner;

public class Handson {
	public static void centraggio(int spazi) {
		for (int i = 0; i < spazi; i++)
		{
			System.out.print(" ");
		}
	}
	
	public static void StampastampaBase(int lung) {
		System.out.print("+");
		for (int i = 0; i < lung; i++)
		{
			System.out.print("-");
		}
		System.out.print("+\n");
	}
	
	public static void stampaRettangolo(String nome) {
		
		int spazi = 10;
		int lunghezza_nome = nome.length();
		
		StampastampaBase(spazi*2+lunghezza_nome);
		
		System.out.print("|");
		centraggio(spazi);
		System.out.print(nome);
		centraggio(spazi);
		System.out.print("|\n");
		
		StampastampaBase(spazi*2+lunghezza_nome);
	}

	public static void main(String[] args) {
		
		/*
		* Creare, tramite l'utilizzo di SCANNER E FUNZIONI, un piccolo programma che dato in input il
		* proprio nome, restituisca in console una formattazione di questo genere:
		*
		* +------------------------------------+
		* |          Giovanni Pace             |
		* +------------------------------------+
		*
		* Challenge: La formattazione deve restituire un rettangolo!!!!
		*/

		Scanner interceptor = new Scanner(System.in);
		
		System.out.println("Inserire il proprio nominativo:");
		String nome = interceptor.nextLine();
		
		stampaRettangolo(nome);
		
		interceptor.close();
		
		
		

	}

}
//soluzione
//		 /**
//	     * Funzione che si occupa di contornare il nome sotto forma di stringa!
//	     * @param var_nome
//	     */
//	 public static void contornaNome(String var_nome) {
//	        int sp_laterali = 22;
//	        String risultato = "";
//
//	        risultato += contorno(sp_laterali, var_nome);       // +--------------+
//
//	        risultato += "|" +
//	                    ripetiCarattere(" ", sp_laterali) +
//	                    var_nome +                              // |   Giovanni   |
//	                    ripetiCarattere(" ", sp_laterali) +
//	                    "|\n";
//
//	        risultato += contorno(sp_laterali, var_nome);       // +--------------+
//
//	        System.out.println(risultato);
//	    }
//
//	    public static String contorno(int var_spazi, String var_nom) {
//	        return "+" + ripetiCarattere("-", (var_spazi * 2) + var_nom.length()) + "+\n";
//	    }
//
//	    /**
//	     * Ripete un carattere o una sequenza di caratteri qualsiasi per un numero definito di volte
//	     * @param var_carattere Carattere da ripetere
//	     * @param var_numc  Numerio di volte che devo ripetere il carattere
//	     * @return  Output una stringa con il carattere ripetuto n volte
//	     */
//	    public static String ripetiCarattere(String var_carattere, int var_numc) {
//	        String frase = "";
//	        for(int i=0; i<var_numc; i++) {
//	            frase += var_carattere;
//	        }
//
//	        return frase;
//	    }
//
//	    public static void main(String[] args) {
//
//	        /*
//	         * Creare, tramite l'utilizzo di SCANNER E FUNZIONI, un piccolo programma che dato in input il
//	         * proprio nome, restituisca in console una formattazione di questo genere:
//	         *
//	         * +------------------------------------+
//	         * |           Mario ROSSI            |
//	         * +------------------------------------+
//	         *
//	         * Challenge: La formattazione deve restituire un rettangolo!!!!
//	         */
//
//	        Scanner interceptor = new Scanner(System.in);
//
//	        System.out.println("Inserisci il nome:");
//	        String nome = interceptor.nextLine();
//	        contornaNome(nome);
//
//	        interceptor.close();
//	    }
//	    }

