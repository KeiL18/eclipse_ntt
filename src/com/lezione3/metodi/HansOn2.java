package com.lezione3.metodi;

import java.util.Scanner;

public class HansOn2 {
	
	/* Creare un sistema che: dati n numeri in input, calcoli la somma di questi numeri.
	 * Effettuare la terminazione dell'input all'inserimento del carattere Q.
	 */
		 public static int calcolaSomma(String var_input) {
		        String[] input_array = var_input.split(";");

		        int sommatoria = 0;

		        for(int i=0; i<input_array.length; i++) {
		            if(!input_array[i].isEmpty()) {
		                sommatoria += Integer.parseInt(input_array[i]);
		            }
		        }

		        return sommatoria;
		    }

		    public static void main(String[] args) {

		        Scanner interceptor =  new Scanner(System.in);

		        boolean inserimento_ok = true;
		        String storico = "";

		        while(inserimento_ok) {
		            System.out.println("Inserisci il numero da sommare: ");
		            String input = interceptor.nextLine();

		            if(input.equals("Q") || input.isBlank()) {
		                int somma = calcolaSomma(storico);
		                System.out.print("La somma �: " + somma);
		                inserimento_ok = false;
		            }
		            else {
		                storico += input + ";";                     // 5;89;1;6;3;
		            }
		        }


		        interceptor.close();

		    }

		}