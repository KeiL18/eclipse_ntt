package com.lezione3.metodi;

public class MetodiComplessi {

    /**
     * Funzione di stampa
     * @param var_array Input di un array di stringhe
     * @param var_nome  Nome dell'elenco da visualizzare nella parte alta
     */
    public static void stampaArray(String[] var_array, String var_nome) {
        System.out.println("-------" + var_nome + "------");
        for(int i=0; i<var_array.length; i++) {
            System.out.println(var_array[i]);
        }
        System.out.println("-------------------\n");
    }

    /**
     * Prima tipologia di richiamo di funzione DA funzione, il dividi array, prende in input
     * una frase, la divide ed invoca successivamente la funzione stampaArray(String[])
     * @param var_frase Frase con separatore ;
     */
//  public static void dividiArray(String var_frase) {
//      String[] elenco = var_frase.split(";");
//      stampaArray(elenco, "ELENCO");
//  }

    /**
     * Seconda versione di dividiArray che, al completamento dello split, restituisce in console
     * la lunghezza dell'array appena creato.
     * @param var_frase Frase con separatore ;
     * @return  Array derivante dallo split della frase passata come parametro
     */
    public static String[] dividiArray(String var_frase) {
        String[] elenco = var_frase.split(";");
        System.out.println("Lunghezza array: " + elenco.length);
        return elenco;
    }

    public static void main(String[] args) {

//      String[] rubrica = {"Giovanni", "Mario", "Valeria", "Marta"};
//      stampaArray(rubrica, "RUBRICA");
//
//      String[] automobili = {"BMW", "FIAT", "Maserati"};
//      stampaArray(automobili, "AUTOMOBILI");


        String frase = "Giovanni;Mario;Marco;Valeria;Federica";
//      String[] elenco = frase.split(";");
//      stampaArray(elenco, "TEST SPLIT");

        //dividiArray(frase);

        //ESTESA
//      String[] rubrica = dividiArray(frase);
//      stampaArray(rubrica, "ELENCO");
        //CONTRATTA
        stampaArray(dividiArray(frase), "ELENCO");
    }

}
