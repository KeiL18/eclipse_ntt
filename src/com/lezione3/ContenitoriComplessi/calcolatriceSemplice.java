package com.lezione3.ContenitoriComplessi;
	import java.lang.reflect.Array;
	import java.util.ArrayList;
	import java.util.Scanner;
	
public class calcolatriceSemplice {



	    /**
	     * Funzione di verifica dell'input che ricade nei casi: +,*,-,/
	     * @param var_input Stringa in formato +numero
	     * @return booleano di compatibilitÓ con le specifiche
	     */
	    public static boolean verificaFormato(String var_input) {

	        char operazione = var_input.charAt(0);

	        switch(operazione) {
	            case '+':
	                return true;
	            case '-':
	                return true;
	            case '*':
	                return true;
	            case '/':
	                return true;
	            default:
	                return false;           //True se vuoi permettere l'inserimento del valore iniziale
	        }

	        //Stesso modo di scrivere la funzione;
//	      boolean risultato = false;
//	      switch(operazione) {
//	          case '+':
//	              risultato = true;
//	              break;
//	          case '-':
//	              risultato = true;
//	              break;
//	          case '*':
//	              risultato = true;
//	              break;
//	          case '/':
//	              risultato = true;
//	              break;
//	      }
	//
//	      return risultato;
	    }

	    public static float pulisciNumero(String var_numero) {
	        return Float.parseFloat(var_numero.replace(" ", "").substring(1));
	    }

	    public static float funzioneSomma(String var_input, float risultato) {
	        float num_pulito = pulisciNumero(var_input);
	        risultato += num_pulito;
	        return risultato;
	    }

	    public static float funzioneDifferenza(String var_input, float risultato) {
	        float num_pulito = pulisciNumero(var_input);
	        risultato -= num_pulito;
	        return risultato;
	    }

	    public static float funzioneProdotto(String var_input, float risultato) {
	        float num_pulito = pulisciNumero(var_input);
	        risultato *= num_pulito;
	        return risultato;
	    }

	    public static float funzioneDivisione(String var_input, float risultato) {
	        float num_pulito = pulisciNumero(var_input);

	        if(num_pulito != 0) {
	            risultato = risultato / num_pulito;
	        }
	        else {
	            System.out.println("[ERR] Errore, divisione per zero non permessa!");
	        }

	        return risultato;
	    }


	    public static float effettuaCalcolo(ArrayList<String> elenco) {
	        float risultato = 0.0f;

	        for(int i=0; i<elenco.size(); i++) {

	            char operazione = elenco.get(i).charAt(0);          //Equivale a scrivere: (elenco.get(i)).charAt(0);
	            switch(operazione) {
	                case '+':
	                    risultato = funzioneSomma(elenco.get(i), risultato);
	                    break;
	                case '-':
	                    risultato = funzioneDifferenza(elenco.get(i), risultato);
	                    break;
	                case '*':
//	                  if(i == 0) {
//	                      risultato = 1;
//	                  }
	                    risultato = funzioneProdotto(elenco.get(i), risultato);
	                    break;
	                case '/':
	                    risultato = funzioneDivisione(elenco.get(i), risultato);
	                    break;
//	              default:                                                                                //Decommenta se vuoi permettere l'inserimento del valore iniziale
//	                  risultato = Float.parseFloat(elenco.get(i).replace(" ", ""));
	            }

	        }

	        return risultato;
	    }



	    public static void main(String[] args) {

	        /*
	         * CREARE UN SISTEMA CHE:
	         * Tramite lo Scanner prende in input n valori, l'input viene terminato solo dall'inserimento
	         * del carattere Q.
	         * Dopo l'inserimento del mio valore, devo effettuare la scelta dell'operazione aritmetica
	         * da effettuare (a scelta tra Addizione, Sottrazione, Moltiplicazione e !!!DIVISIONE!!!).
	         *
	         * Ad ogni iterazione, viene effettuata la stampa del risultato e dello storico dell'operazione
	         * rispettando il formato:
	         *
	         * 5 + 2 - 1 =
	         * 6
	         */

	        Scanner interceptor = new Scanner(System.in);

	        int inserimento_ok = 1;             //Equivale al True
	        ArrayList<String> elenco = new ArrayList<String>();
	        float risultato = 0;

	        while(inserimento_ok != 0) {

	            System.out.println("Inserisci l'operazione da effettuare, premi Q per terminare l'operazione:");
	            String input = interceptor.nextLine();

	            if(input.equals("Q")) {
	                inserimento_ok = 0;         //Equivale al False
	            }
	            else {
	                if(verificaFormato(input) == true) {                    //Verifica la correttezza dell'input
	                    elenco.add(input);

	                    risultato = effettuaCalcolo(elenco);
	                    System.out.println("=" + risultato);
	                }
	                else {
	                    System.out.println("[ERR] Errore di inserimento");
	                }

	            }
	        }

	        interceptor.close();

	        System.out.println("Programma terminato!");

	    }

	}