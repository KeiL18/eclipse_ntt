package com.lezione3.ContenitoriComplessi;

import java.util.ArrayList;

public class ContenitoriComplessi {

	public static void main(String[] args) {

		
//    String[] macchine = {"BMW", "FIAT", "Maserati"};    //Dichiara la dimensione on-fly
//
//  String[] macchine =  new String[3];
//  macchine[0] = "BMW";
//  macchine[1] = "FIAT";
//  macchine[2] = "Maserati";
////    macchine[3] = "Maserati";   //OUT OF BOUNDS
//
//  System.out.println(macchine.length);
//
//  String nome = "Gio";
//  System.out.println(nome.length());

    //ArrayList

    ArrayList<String> pippo = new ArrayList<String>();

    pippo.add("Giovanni");
    pippo.add("Mario");
    pippo.add("Valeria");

    pippo.remove(1);

    for(int i=0; i<pippo.size(); i++) {
        System.out.println(pippo.get(i));
    }


	}

}
