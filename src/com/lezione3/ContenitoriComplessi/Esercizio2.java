package com.lezione3.ContenitoriComplessi;

import java.util.ArrayList;
import java.util.Scanner;

public class Esercizio2 {
	 
	/*
     * CREARE UN SISTEMA CHE:
     * Tramite lo Scanner prende in input n valori, l'input viene terminato solo dall'inserimento
     * del carattere Q.
     * Dopo l'inserimento del mio valore, devo effettuare la scelta dell'operazione aritmetica
     * da effettuare (a scelta tra Addizione e Sottrazione).
     *
     * Ad ogni iterazione, viene effettuata la stampa del risultato e dello storico dell'operazione
     * rispettando il formato:
     *
     * 5 + 2 - 1 =
     * 6
     */
	
	public static float calcolatore(ArrayList<String> var_input) {

        float totale = 0;

        for(int i=0; i<var_input.size(); i++) {
        	switch (var_input.get(i).substring(0,1)) {
    			case "+":
        			totale += Integer.parseInt(var_input.get(i).substring(1).trim());
        			break;
    			case "-":
	        		totale -= Integer.parseInt(var_input.get(i).substring(1).trim());
	    			break;
    			case "*":
	        		totale *= Integer.parseInt(var_input.get(i).substring(1).trim());
	    			break;
    			case "/":
    				if (var_input.get(i).substring(1).trim() != "0") 
    					totale /= Integer.parseInt(var_input.get(i).substring(1).trim());
    				else
    					System.out.println("[ERR] Impossibile");	        		
	    			break;
	    		default:
	    			totale = Integer.parseInt(var_input.get(i).trim());
        	}
        		
        }

        return totale;
    }
	
	public static String cronologia(ArrayList<String> storia) {
		
		String cronologia = "";
		for(int i=0; i<storia.size(); i++) {
			
			if (!(storia.get(i).contains("+")) && i == 0)
				cronologia += storia.get(i).substring(0, 1) + " " + storia.get(i).substring(1).trim();
			else
				cronologia += " " + storia.get(i).substring(0, 1) + " " + storia.get(i).substring(1).trim();
            if (i == (storia.size()-1))
            	cronologia += " =";
		}
		
		return cronologia;
	}
	
	public static void stampaRisultato(ArrayList storico) {
		float totale = calcolatore(storico);
		
		System.out.println("----------------------");
		System.out.print(cronologia(storico) + "\n" + totale + "\n");
		System.out.println("----------------------");
	}
	
	public static void main(String[] args) {

		Scanner interceptor =  new Scanner(System.in);

        boolean inserimento_ok = true;
        ArrayList<String> storico = new ArrayList<String>();
        
        while(inserimento_ok) {
            System.out.println("Inserisci l'operazione da eseguire (Q per terminare): ");
            String input = interceptor.nextLine();
            if(input.toUpperCase().trim().equals("Q") || input.isBlank()) {
                inserimento_ok = false;
            	}
            else {
                storico.add(input); 
            	}
		
            stampaRisultato(storico);			//restituisce il risultato ad ogni inserimento
            
        }
        
//      stampaRisultato(storico);			//restituisce il risultato alla fine
        
        System.out.println("---------- PROGRAMMA TERMINATO ----------");
        interceptor.close();
	}

}
