package com.lezione3.ContenitoriComplessi;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class ContenitoriComplessi2 {

    public static void stampaStorico(ArrayList<String> var_cont) {
        String str_storico = "";

        for(int i=0; i<var_cont.size(); i++) {
             str_storico += var_cont.get(i);

             if(i < var_cont.size() - 1) {
                 str_storico += "+";
             }
        }

        System.out.println(str_storico);
    }

    public static int sommaContenitore(ArrayList<String> var_cont) {
        int somma = 0;

        for(int i=0; i<var_cont.size(); i++) {
            somma += Integer.parseInt(var_cont.get(i));
        }

        return somma;
    }

    public static void aggiungiAContenitore(String val_input, ArrayList<String> val_cont) {
        if(!val_input.isEmpty()) {
            val_cont.add(val_input);
        }
        else {
            System.out.println("Attenzione, valore non aggiunto, hai inserito un input non valido!");
        }
    }

    public static void main(String[] args) {

////        String[] macchine = {"BMW", "FIAT", "Maserati"};    //Dichiara la dimensione on-fly
//
//      String[] macchine =  new String[3];
//      macchine[0] = "BMW";
//      macchine[1] = "FIAT";
//      macchine[2] = "Maserati";
////        macchine[3] = "Maserati";   //OUT OF BOUNDS
//
//      System.out.println(macchine.length);
//
//      String nome = "Gio";
//      System.out.println(nome.length());

        //ArrayList

//      ArrayList<String> pippo = new ArrayList<String>();
//
//      pippo.add("Giovanni");
//      pippo.add("Mario");
//      pippo.add("Valeria");
//
//      pippo.remove(1);
//
//      for(int i=0; i<pippo.size(); i++) {
//          System.out.println(pippo.get(i));
//      }

        /*
         * Creare un sistema che: Dati n numeri in input, calcoli la somma di questi numeri.
         * Effettuare la terminazione dell'input all'inserimento del carattere Q
         * La somma viene effettuata ad ogni inserimento
         * Al calcolo della somma, verr� restituito anche lo storico delle operazioni, es:
         *
         * 5 + 6 + 1 =
         * 12
         *
         * N.B. Rispettare il formato ed utilizzare ArrayList!
         */

        Scanner interceptor = new Scanner(System.in);
        boolean inserimento_ok = true;
        ArrayList<String> contenitore = new ArrayList<String>();


        while(inserimento_ok) {
            System.out.println("Inserisci il valore:");
            String input = interceptor.nextLine();

            if(input.equals("Q")) {
                inserimento_ok = !inserimento_ok;       //Cambia il valore booleano al suo opposto!
            }
            else {
                aggiungiAContenitore(input, contenitore);
                int risultato = sommaContenitore(contenitore);
                stampaStorico(contenitore);
                System.out.println("La somma �: " + risultato);
            }
        }

        interceptor.close();

    }

    /*
     * CREARE UN SISTEMA CHE:
     * Tramite lo Scanner prende in input n valori, l'input viene terminato solo dall'inserimento
     * del carattere Q.
     * Dopo l'inserimento del mio valore, devo effettuare la scelta dell'operazione aritmetica
     * da effettuare (a scelta tra Addizione e Sottrazione).
     *
     * Ad ogni iterazione, viene effettuata la stampa del risultato e dello storico dell'operazione
     * rispettando il formato:
     *
     * 5 + 2 - 1 =
     * 6
     */

}
