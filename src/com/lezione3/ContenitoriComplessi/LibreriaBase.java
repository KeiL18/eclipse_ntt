package com.lezione3.ContenitoriComplessi;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class LibreriaBase {

    public static void main(String[] args) {

        /**
         * Data la seguente libreria:
            String[] libro_1 = {"Storia della buonanotte", "Favilli", "12345-12345-12345"};
            String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", "432434-43423-43243"};
            String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
            String[] libro_4 = {"Storia del Buongiorno", "Favilli", "12345-12345-12645"};
            String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
            String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};



            1. Contare tutti i libri di Veltroni e di Favilli -> OK

            2. Crea uno scanner che permette l'inserimento di un nuovo libro. -> OK

            3. Dare la possibilit� di effettuare il conteggio dei libri inserendo l'autore in console. -> OK

            4. Stampa dei dettagli di un libro ricercato per ISBN

            5. Stampa dei dettagli di pi� libri ricercati per autore

            6. Eliminazione di un libro per ISBN

            7. Modifica del titolo di un libro dopo aver dato l'ISBN
         */

        String[] libro_1 = {"Storia della buonanotte", "Favilli", "12345-12345-12345"};
        String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", "432434-43423-43243"};
        String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
        String[] libro_4 = {"Storia del Buongiorno", "Favilli", "12345-12345-12645"};
        String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
        String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};

        ArrayList<String[]> elenco_libri = new ArrayList<String[]>();
        elenco_libri.add(libro_1);
        elenco_libri.add(libro_2);
        elenco_libri.add(libro_3);
        elenco_libri.add(libro_4);
        elenco_libri.add(libro_5);
        elenco_libri.add(libro_6);

//      int contatore_vel = contaLibri(elenco_libri, "Veltroni");
//      int contatore_fav = contaLibri(elenco_libri, "Favilli");
//
//      System.out.println("Contatore Veltroni: "+ contatore_vel);
//      System.out.println("Contatore Favilli: "+ contatore_fav);

        Scanner interceptor = new Scanner(System.in);

        boolean disabilita_inserimento = false;
        while(!disabilita_inserimento) {
            System.out.println("Cosa vuoi fare?\n"
                    + "Q - QUIT\n"
                    + "I - Inserisci\n"
                    + "R - Ricerca\n"
                    + "D - Elimina\n"
                    + "C - Conta\n");

            String input = interceptor.nextLine();

            switch(input) {
            case "Q":
                disabilita_inserimento = true;
                break;
            case "I":
//              System.out.println("Inserisci il titolo:");
//              String var_titolo = interceptor.nextLine();
//              System.out.println("Inserisci il autore:");
//              String var_autore = interceptor.nextLine();
//              System.out.println("Inserisci il ISBN:");
//              String var_isbn = interceptor.nextLine();
//
//              if(inserisciLibro(elenco_libri, var_titolo, var_autore, var_isbn)) {
//                  System.out.println("Inserimento effettuato con successo!");
//              }

                String[] libro_nuovo = new String[3];
                boolean inserimento_bilitato = true;

                String[] domande = {"Inserisci il titolo:", "Inserisci il autore:", "Inserisci il ISBN:"};
//              for(int i=0; i<domande.length; i++) {
//                  System.out.println(domande[i]);
//                  libro_nuovo[i] = interceptor.nextLine();
//
//                  if(libro_nuovo[i].isEmpty()) {
//                      inserimento_bilitato = false;
//                      i = domande.length;
//                  }
//              }

                int i=0;
                while(i<domande.length && inserimento_bilitato) {
                    System.out.println(domande[i]);
                    libro_nuovo[i] = interceptor.nextLine();

                    if(libro_nuovo[i].isEmpty()) {
                        inserimento_bilitato = false;
                    }

                    i++;
                }

                if(inserimento_bilitato)
                    elenco_libri.add(libro_nuovo);

                break;
            case "C":
                System.out.println("Inserisci il nome:");
                String autore = interceptor.nextLine();

                int libri_contati = contaLibri(elenco_libri, autore);
                System.out.println("In elenco ci sono " + libri_contati + " libri.\n");
                break;
            case "R":
                System.out.println("Come vuoi effettuare la ricerca?\nA - AUTORE\nI - ISBN\n");
                String scelta = interceptor.nextLine();
                System.out.println("Inserisci il valore da ricercare:");
                String input_ricerca = interceptor.nextLine();

                switch(scelta) {
                case "A":
                    ricercaLibro(elenco_libri, "R-AUTORE", input_ricerca);
                    break;
                case "I":
                    ricercaLibro(elenco_libri, "R-ISBN", input_ricerca);
                    break;
                }
                break;
            case "D":
                System.out.println("Inserisci l'ISBN da eliminare:");
                String isbn_eliminazione = interceptor.nextLine();

                ricercaLibro(elenco_libri, "D-ISBN", isbn_eliminazione);
                break;
            }
        }

        interceptor.close();

    }

    /**
     * Funzione di ricerca generica
     * @param var_elenco    Elenco dove verr� effettuata la ricerca
     * @param var_tipo      Variabile che indica il campo di ricerca, possibili scelte R-AUTORE||R-ISBN||D-ISBN
     * @param var_input
     */
    public static void ricercaLibro(ArrayList<String[]> var_elenco, String var_tipo, String var_input) {

        for(int i=0; i<var_elenco.size(); i++) {

            String[] lib_temp = var_elenco.get(i);

            switch(var_tipo) {
                case "R-AUTORE":
                    if(lib_temp[1].equals(var_input)) {
                        stampaLibro(lib_temp);
                    }
                    break;
                case "R-ISBN":
                    if(lib_temp[2].equals(var_input)) {
                        stampaLibro(lib_temp);
                    }
                    break;
                case "D-ISBN":
                    if(lib_temp[2].equals(var_input)) {
                        var_elenco.remove(i);
                        System.out.println("Done!");
                    }
                    break;
                }
        }

    }

    public static void stampaLibro(String[] var_libro) {
        System.out.println("" + var_libro[0] + ";" + var_libro[1] + ";" + var_libro[2]);
    }


    public static boolean inserisciLibro(ArrayList<String[]> var_elenco, String var_titolo, String var_autore, String var_isbn) {

//      String[] libro_nuovo = {var_titolo, var_autore, var_isbn};
        String[] libro_nuovo = new String[3];
        libro_nuovo[0] = var_titolo;
        libro_nuovo[1] = var_autore;
        libro_nuovo[2] = var_isbn;

        var_elenco.add(libro_nuovo);

        return true;
    }

    public static int contaLibri(ArrayList<String[]> elenco, String var_nome) {

        int contatore = 0;

        for(int i=0; i<elenco.size(); i++) {
            String[] libro_temp = elenco.get(i);

            if(libro_temp[1].equalsIgnoreCase(var_nome))
                contatore++;
        }

        return contatore;
    }

}
