package com.lezione3.ContenitoriComplessi;

import java.util.ArrayList;
import java.util.Scanner;

public class ScansioneArray {

	public static void main(String[] args) {
		
	    /* Data la seguente libreria:
	        String[] libro_1 = {"Storia della buonanotte", "Favilli", "12345-12345-12345"};
	        String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", "432434-43423-43243"};
	        String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
	        String[] libro_4 = {"Storia del Buongiorno", "Favilli", "12345-12345-12645"};
	        String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
	        String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};
	        
            1. Contare tutti i libri di Veltroni e di Favilli -> OK

            2. Crea uno scanner che permette l'inserimento di un nuovo libro.

            3. Dare la possibilit� di effettuare il conteggio dei libri inserendo l'autore in console. -> OK
            
            4. Stampa dei dettagli di un libro ricercato per ISBN

            5. Stampa dei dettagli di pi� libri ricercati per autore

            6. Eliminazione di un libro per ISBN

            7. Modifica del titolo di un libro dopo aver dato l'ISBN

	     */

		boolean funziona = true;
		String autore = "", lettura = "";
		String[] info = {"", "", ""};
		ArrayList<String[]> catalogo = new ArrayList<String[]>();
		Scanner interceptor = new Scanner(System.in);
		
		String[] libro_1 = {"Storia della buonanotte", "Favilli", "12345-12345-12345"};
        String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", "432434-43423-43243"};
        String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
        String[] libro_4 = {"Storia del Buongiorno", "Favilli", "12345-12345-12645"};
        String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
        String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};
		
        catalogo.add(libro_1); catalogo.add(libro_2); catalogo.add(libro_3);
        catalogo.add(libro_4); catalogo.add(libro_5); catalogo.add(libro_6);
		
        while (funziona) {
        	System.out.println("---------- NUOVA RICHIESTA ----------");
            System.out.println("Scegliere l'operazione da eseguire: \n"
            		+ " A - Aggiungere libri al catalogo\n"
            		+ " C - Verificare quanti libri per un autore\n"
            		+ " SI - Stampa dettagli libro da ISBN\n"
            		+ " SA - Stampa dettagli libri di un autore\n"
            		+ " D - Eliminazione di un libro per ISBN\n"
            		+ " M - Modifica di un libro per ISBN\n"
            		+ " B - per il funzionamento di base (Favilli e Veltroni)\n"
            		+ " Q - Per uscire");
            
            int linea = 0;

            String scelta = interceptor.nextLine();
            
            switch (scelta) {
            	case "A":
            		System.out.println("Inserire il nome del libro:");
            		String libro = interceptor.nextLine();
            		System.out.println("Inserire l'autore:");
            		autore = interceptor.nextLine();
            		System.out.println("Inserire codice ISBN:");
            		String ISBN = interceptor.nextLine();
            		
            		aggiunta(catalogo, libro, autore, ISBN);
            		break;
            	case "C":
            		System.out.println("Inserire l'autore di cui si cercano i titoli:");
            		autore = interceptor.nextLine().toLowerCase();
            		int contatore = conta(catalogo, autore);
            		System.out.println("Risultano " + contatore + " libri di " + autore + " a catalogo.");
            		break;
            	case "SI":
            		System.out.println("Inserire l'ISBN di cui si vuole effetuare la ricerca:");
            		lettura = interceptor.nextLine();
            		
            		linea = ricerca(catalogo, lettura, 2, 0);
            		if (linea != -1) {
            			info = catalogo.get(linea);
            			System.out.println("TITOLO DEL LIBRO: " + info[0] + "\nAUTORE DEL LIBRO:" + info[1]);
            		}
            		else
            			System.out.println("ISBN non trovato.");
            		
            		break;
            	case "SA":
            		//TODO: non funziona ancora
            		System.out.println("Inserire l'autore di cui si vuole effetuare la ricerca:");
            		lettura = interceptor.nextLine();
            		
            		for (int i = 0; i < catalogo.size(); i++) {
            			linea = ricerca(catalogo, lettura, 1, i);
            			if (linea != -1) {
            				info = catalogo.get(linea);
            				System.out.println("TITOLO DEL LIBRO: " + info[0] + "\nAUTORE DEL LIBRO:" + info[2]);
            				}
            			else
            				System.out.println("Autore non trovato.");
            			}

            		break;
            	case "D":
            		System.out.println("Inserire l'ISBN di cui si vuole effetuare la cancellazione:");
            		lettura = interceptor.nextLine();
            		
            		linea = ricerca(catalogo, lettura, 2, 0);
            		if (linea != -1)
            			catalogo.remove(linea);
            		else
            			System.out.println("ISBN non trovato.");
            		
            		break;
            	case "M":
            		System.out.println("Inserire l'ISBN di cui si vuole effetuare la modifica:");
            		lettura = interceptor.nextLine();
            		
            		linea = ricerca(catalogo, lettura, 2, 0);
            		if (linea != -1) {
            			System.out.println("Quale voce vuoi cambiare:\n"
            					+ " 1 - Titolo\n 2 - Autore");
                		lettura = interceptor.nextLine();
                		switch (lettura) {
                			case "1":
	                			System.out.println("Inserire la modifica:");
	                			//TODO: continua la modifica per il miglioramento
//	                			catalogo = ;
	                			break;
                			case "2":
                				break;
                		}
                		
                		//aggiunta(catalogo, libro, autore, ISBN);
            		}
            		else
            			System.out.println("ISBN non trovato.");
            		break;
            	case "B":
            		int NFavilli = 0 , NVeltroni= 0;
            		NFavilli = conta(catalogo, "Favilli".toLowerCase());
            		NVeltroni = conta(catalogo, "Veltroni".toLowerCase());
        
            		System.out.println("Sono presenti " + NFavilli + " libri di Favilli e " + NVeltroni + " libri di Veltroni.");
            		break;
            	case "Q":
            		funziona = false;
            		System.out.println("---------- PROGRAMMA TERMINATO ----------");
            		break;
            	default: 
            		System.out.println("[ERR] Inserimento non valido, utilizzare una voce valida.");
            }
            
        }
		
		interceptor.close();

	}
	
	/** 
	 * Funzione che ricerca la linea del nostro miniDB in cui � memorizzato il libro ricercato e salva tutte le sue informazioni in una stringa di stringhe
	 * @param registro DB
	 * @param fattore fattore di ricerca
	 * @param colonnaRicerca posizione fattore di ricerca
	 * @return riga del DB
	 */
	public static int ricerca(ArrayList<String[]> registro, String fattore, int colonnaRicerca, int partenza) {
		int riga = partenza;
		boolean trovato = false;
		
		for (int i = partenza; i < registro.size(); i++) {
			String[] mem = registro.get(i);
			if (mem[colonnaRicerca].toLowerCase().equals(fattore)) {
				riga = i;
				trovato = true;
				i = registro.size();
			}
		}
		
		if (!trovato && partenza == 0)
			riga = -1;
		
		return riga;
	}
	
	/**
	 * Funzione che si occupa della ricerca del cognome nel registro di libri caricato, andandone a valutare la ricorrenza
	 * @param registro catalogo da analizzare
	 * @param nome autore da ricercare
	 * @return numero di libri dell'autore a catalogo
	 */
	public static int conta(ArrayList<String[]> registro, String nome) {
		int risultati = 0;
		
		for (int i= 0; i < registro.size(); i++) {
			String[] memo = registro.get(i);
				if (memo[1].toLowerCase().equals(nome))
					risultati++;
		}
		return risultati;
	}
	
	/**
	 * Funzione che si occupa dell'aggiunta del libro a catalogo, dopo aver verificato l'inserimento di valori non nulli.
	 * @param catalogo catalogo dei libri come Arraylist
	 * @param libro titolo del libro che si vuole aggiugere
	 * @param autore autore del libro
	 * @param codice ISBN
	 */
	public static void aggiunta(ArrayList<String[]> catalogo, String libro, String autore, String codice) {
		if (libro.isBlank() || autore.isBlank() || autore.isBlank())
			System.out.println("[ERR] Errore nella compilazione delle informazioni. Il libro non � stato aggiunto.");
		else {
			String[] aggiunta = {libro, autore, codice};
			catalogo.add(aggiunta);
		}
			
	}

}
