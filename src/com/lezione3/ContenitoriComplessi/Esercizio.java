package com.lezione3.ContenitoriComplessi;

import java.util.ArrayList;
import java.util.Scanner;

public class Esercizio {

		
		/*
         * Creare un sistema che: Dati n numeri in input, calcoli la somma di questi numeri.
         * Effettuare la terminazione dell'input all'inserimento del carattere Q
         * La somma viene effettuata ad ogni inserimento
         * Al calcolo della somma, verr� restituito anche lo storico delle operazioni, es:
         *
         * 5 + 6 + 1 =
         * 12
         *
         * N.B. Rispettare il formato ed utilizzare ArrayList!
         */
		
		public static int calcolaSomma(ArrayList<String> var_input) {

	        int sommatoria = 0;

	        for(int i=0; i<var_input.size(); i++) {
	                sommatoria += Integer.parseInt(var_input.get(i));
	        }

	        return sommatoria;
	    }
		public static String cronologia(ArrayList<String> storia) {
			
			String cronologia = "";
			for(int i=0; i<storia.size(); i++) {
                cronologia += storia.get(i);
                if (i < (storia.size()-1))
                	cronologia += " + ";
                else
                	cronologia += " =";
			}
			
			return cronologia;
		}

	    public static void main(String[] args) {

	        Scanner interceptor =  new Scanner(System.in);

	        boolean inserimento_ok = true;
	        ArrayList<String> storico = new ArrayList<String>();

	        while(inserimento_ok) {
	            System.out.println("Inserisci il numero da sommare: ");
	            String input = interceptor.nextLine();
	            if(input.toUpperCase().equals("Q") || input.isBlank()) {
	                inserimento_ok = false;
	            }
	            else {
	            	if (!input.isBlank()) {
	                storico.add(input); 
	            	}
	            }
	        int somma = calcolaSomma(storico);
			System.out.println("----------------------");
			System.out.print(cronologia(storico) + "\n" + somma + "\n");
	        System.out.println("----------------------");
	        }
	        
	        interceptor.close();
	}

}
