package com.lezione3.ContenitoriComplessi;

import java.util.ArrayList;

public class TestContenitori {
    //                                              var_array = elenco_uno
    public static void stampaArrayList(ArrayList<String> var_array, String var_nome) {

        System.out.println("---" + var_nome + "---");
        for(int i=0; i<var_array.size(); i++) {
            System.out.println(var_array.get(i));
        }
        System.out.println("---------------");
    }

    public static void main(String[] args) {

//      ArrayList<String> elenco_uno = new ArrayList<String>();
//      elenco_uno.add("GIOVANNI");
//      elenco_uno.add("MARIO");
//
//      stampaArrayList(elenco_uno, "UNO");
//
//      ArrayList<String> elenco_due = elenco_uno;
//      stampaArrayList(elenco_due, "DUE");
//
//      elenco_uno.add("VALERIO");
//      stampaArrayList(elenco_due, "DUE");
//
//      elenco_due.add("FLORIAN");
//      stampaArrayList(elenco_uno, "UNO");

        ArrayList<String> rubrica_telefonica = new ArrayList<String>();
        rubrica_telefonica.add("GIOVANNI");
        rubrica_telefonica.add("MARIA");
        rubrica_telefonica.add("VALERIA");
        rubrica_telefonica.add("MARCO");

        ArrayList<String> backup_rubrica = new ArrayList<String>();
        backup_rubrica = rubrica_telefonica;
        stampaArrayList(backup_rubrica, "BACKUP");

        rubrica_telefonica.add("MIRKO");
        stampaArrayList(backup_rubrica, "BACKUP");


    }

}
