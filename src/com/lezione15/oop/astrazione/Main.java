package com.lezione15.oop.astrazione;

public class Main {

	public static void main(String[] args) {

		Gatto gattila = new Gatto();
		gattila.versoEmesso();
		
		gattila.setNumeroZampe(4);
		System.out.println(gattila.getNumeroZampe());
				
	}

}
