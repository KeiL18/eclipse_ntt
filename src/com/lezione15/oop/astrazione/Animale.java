package com.lezione15.oop.astrazione;

public abstract class Animale {

	protected int numero_zampe;
	
	public abstract void versoEmesso();
	
	public void setNumeroZampe(int var_zampe) {
		this.numero_zampe = var_zampe;
	}
	public int getNumeroZampe() {
		return this.numero_zampe;
	}
	
}
