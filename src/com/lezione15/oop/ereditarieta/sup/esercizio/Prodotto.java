package com.lezione15.oop.ereditarieta.sup.esercizio;

public class Prodotto {
	
//	protected String tipologia;
	protected String marca;
	protected float prezzo;
	protected String colore;
	protected int potenza;
	protected String carburante;
	
//	public void setTipologia(String var_tipo) {
//		this.tipologia = var_tipo;
//	}
	
	public void setMarca(String var_marc) {
		this.marca = var_marc;
	}
	
	public void setPrezzo(float var_prez) {
		this.prezzo = var_prez;
	}
	
	public void setColore(String var_colo) {
		this.colore = var_colo;
	}
	
	public void setPotenza(int var_pote) {
		this.potenza = var_pote;
	}
	
	public void setPotenza(String var_carb) {
		this.carburante = var_carb;
	}
//	public String getTipologia() {
//		return this.tipologia;
//	}
	
	public String getMarca() {
		return this.marca;
	}
	
	public float getPrezzo() {
		return this.prezzo;
	}
	
	public String getColore() {
		return this.colore;
	}
	
	public int getPotenza() {
		return this.potenza;
	}
	
	public String getCarburante() {
		return this.carburante;
	}
}
