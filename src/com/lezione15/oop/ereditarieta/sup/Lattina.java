package com.lezione15.oop.ereditarieta.sup;

public class Lattina {

    protected int diametro;
    protected int altezza;
    protected String colore = "blu";

    public void setDiametro(int var_dia) {
        this.diametro = var_dia;
    }
    public void setAltezza(int var_alt) {
        this.altezza = var_alt;
    }
    public void setColore(String var_col) {
        this.colore = var_col;
    }

    public int getDiametro() {
        return this.diametro;
    }
    public int getAltezza() {
        return this.altezza;
    }
    public String getColore() {
        return this.colore;
    }

    public void stampaTipologia() {
        System.out.println("Sono una lattina");
    }
}
