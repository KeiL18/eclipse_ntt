package com.lezione15.oop.ereditarieta;

public class Professore extends Persona {

	private String livello;
	
	public void setLivello(String var_live) {
		this.livello = var_live;
	}
	
	public String getLivello() {
		return this.livello;
	}
	
}
