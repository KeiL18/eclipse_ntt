package com.lezione15.oop.ereditarieta;

public class Main {

	public static void main(String[] args) {
		
		Persona per_1 = new Persona();
		per_1.setNominativo("Giovanni Pace");
//		per_1.setTelefono("789456");
//		per_1.setEmail("giovanni@pace.com");
//		per_1.salvaEta(19);
//		per_1.stampa();
		
//		Studente stud_1 = new Studente();
//		stud_1.setNominativo("Mario Rossi");
//		stud_1.setMatricola("123ABC");
		
//		Studente stud_2 = new Studente("789ABC", "2020-01-01");
//		
		Studente stud_3 = new Studente(
					"Valeria Verdi",
					"123456789",
					"test@test.com",
					"1234567ABC",
					"2020-01-02"
				);
		
		stud_3.stampa();
		
//		Professore prof_1 = new Professore();
//		prof_1.setNominativo("Giorgio Cicinelli");
	}

}
