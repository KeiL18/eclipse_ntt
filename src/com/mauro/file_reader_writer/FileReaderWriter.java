package com.mauro.file_reader_writer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

public class FileReaderWriter {

	public static void main(String[] args) 
	{
		FileWriter fw = null;
		FileReader fr = null;
		try 
		{
			
			File file = new File("esempio.txt");
			fw = new FileWriter(file, true);
			fw.write("KEY1=VALUE1");
			fw.write("\nKEY2=VALUE2");
			fw.flush();
			
			if(file.exists())
			{
			  			   
			   BufferedReader in = new BufferedReader(new FileReader(file));
			   String line ="";
			   while ((line = in.readLine()) != null) 
			   {
				   System.out.println(line);
			   }
			   
			   in.close();
				
				
			}
			   
		
		} 
		catch (IOException e) 
		{
			System.err.println("errore");
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		

	}

}