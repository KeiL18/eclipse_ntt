package com.mauro.versioning;

import java.nio.file.FileSystems;

public class Start {

	public static void main(String[] args) {

	}

	public String generaLinea(String var_nome, String var_cognome, String var_data, String var_luogo) {
		String linea = "";
		linea += var_nome;
		linea += spaziatura(var_nome, 25);
		linea += var_cognome;
		linea += spaziatura(var_cognome, 25);
		linea += var_data;
		linea += spaziatura(var_data, 10);
		linea += var_luogo;
		linea += spaziatura(var_luogo, 25);
		linea += "\n";
		
		return linea;
	}
	
	public String spaziatura(String var_testo, int larghezza_colonna) {
		String spazi = "";
		for(int i = 0; i <(larghezza_colonna - var_testo.length()); i++) {
			spazi += " ";
		}
		return spazi;
	}
}
