package com.mauro.multithread_1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Calcolator extends Thread implements Runnable{

	private int numero;
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	Calcolator(String var_num){
		setNumero(Integer.parseInt(var_num));
	}
	
	public void run() {
		FileWriter scrittura = null;
		
		try {
			File file = new File("Tabellina_del_" + this.numero +  ".txt");
			scrittura = new FileWriter(file, false);
			scrittura.write("Ecco la tabellina del " + this.numero
					+ "\n -----------------------------------\n");
			
			for(int i = 0; i <= 10; i++) {
				scrittura.write("          " + (this.numero * i) + "\n"  );
			}
			
			scrittura.flush();
		}catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				scrittura.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
