package com.mauro.multithread_1;

import java.util.ArrayList;
import java.util.Scanner;

public class Start {

	public static void main(String[] args) {

		/*
		 * Progetto Tabelline In consegna entro Domenica 21-02-21 ore 23.59 Creare un
		 * programma che chieda (tramite Scanner) quante tabelline elaborare (es.3).
		 * Successivamente chiedere i valori delle tabelline da elaborare sino a x10
		 * (es, 7,8,11) Per ogni tabellina generare tanti file quanti sono le tabelline
		 * da elaborare e scriverci all�interno lo sviluppo della tabellina (si
		 * consiglia l'uso di Filewriter ma up2u). Per l'esportazione usare il
		 * multithreading per generare in parallelo i file richiesti.
		 */

		Scanner lettore_input = new Scanner(System.in);
		int quantita_tabelline = 0, i = 0, j = 0;
		ArrayList<String> tabelline = new ArrayList<String>();
		boolean inserimento = true;
		
		System.out.println(" ---------- CALCOLATORE DI TABELLINE ----------\n\n"
				+ "Il programma restituisce dei file in formato .txt contenenti le tabelline richieste (al massimo 100).\n\n"
				+ "Ogni volta che viene inserito un comando � necessario premere invio.\n\n"
				+ "---------------------------------------------------------------------------------------\n");

		// loop per l'inserimento del numero di tabelline da elaborare, finche non si inserisce un carattere valido o un numero (massimo 100)
		while (inserimento) {
			String var_temp = "";
			System.out.println("Quante tabelline vuoi elaborare? (Premere Q per uscire dal programma)");
			var_temp = lettore_input.nextLine();

			if (var_temp.toUpperCase().equals("Q")) {
				System.out
				.println("\n---------------------------------------------------------\n" + "Il programma � stato chiuso.\n");
				return;
			} 
			if (Integer.parseInt(var_temp) <= 100) {
				try {
					quantita_tabelline = Integer.parseInt(var_temp);
					inserimento = false;
				} catch (Exception e) {
					System.out.println("Inserimento non valido\n" + "----------------------------------");
				}
			}
			else {
				System.out.println("Inserimento non valido\n" + "----------------------------------");
			}
		}

		inserimento = true;
		//loop per l'inserimento di tutte le tabelline di cui si vuole effettuare il calcolo
		while (inserimento) {
			String var_temp = "";
			System.out.println("Inserire la tabellina di cui si vuole eseguire il calcolo (Q per concludere l'inserimento):");
			var_temp = lettore_input.nextLine();

			if (var_temp.toUpperCase().equals("Q")) {
				quantita_tabelline = i; //se si decide di non inserire un numero di tabelline pari a quelle indicate in precedenza, ne ridefinisce la quanti�
				inserimento = false;
			} else {
				try {
					int tentativo = Integer.parseInt(var_temp); //utile, ma poco elegante per verificare che venga inserito un numero
					tabelline.add(var_temp);
					i++;
				} catch (Exception e) {
					System.out.println("Inserimento non valido\n" + "----------------------------------");
				}
			}
			//controllo di aver raggiunto il numero di tabelline indicato in precedenza
			if (i == quantita_tabelline) {
				inserimento = false;
			}
		}

		lettore_input.close();

		//loop per il calcolo delle tabelline indicate. Eseguito in multithread
		while (j < quantita_tabelline) {
			Calcolator tabellina = new Calcolator(tabelline.get(j));
			tabellina.start();
			j++;
		}

		System.out
				.println("\n---------------------------------------------------------\n" + "Il programma ha terminato tutte le operazioni.\n"
						+ "\nPuoi trovare tutte le tabelline eseguite qui: " + System.getProperty("user.dir"));
	}
}
