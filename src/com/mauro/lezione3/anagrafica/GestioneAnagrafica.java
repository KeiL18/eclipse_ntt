package com.mauro.lezione3.anagrafica;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GestioneAnagrafica {

	private String workingDir = System.getProperty("user.dir");
	public Persona personaTemp;
	private File DataBase = new File(workingDir + File.separator+"anagrafica.txt");
	
	/**
	 * Metodo per l'inserimento di una persona all'interno dell'anagrafica
	 * @param varPersona Persona da inserire
	 */
	public void inserimentoPersona(Persona varPersona) {
		FileWriter anagrafica = null;
		String lineaFile = "";
		
		lineaFile = generaLinea(varPersona);
		try {
			anagrafica = new FileWriter(DataBase,true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			anagrafica.write(lineaFile);
			anagrafica.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			anagrafica.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stampaLista() {
		String linea = "";
		
		System.out.println("-----------------------------------------------------------------------------------------------------\n"
				+ "NOME                                    COGNOME             DATA           LUOGO DI NASCITA");
		
		BufferedReader lettore = null;
		try {
			lettore = new BufferedReader(new FileReader(DataBase));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		try {
			while ((linea = lettore.readLine()) != null) {
				System.out.println(linea);
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		System.out.println("-----------------------------------------------------------------------------------------------------\n");
		
		try {
			lettore.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo per la ricerca degli utenti in linea
	 * @param varCognome stringa da ricercare
	 * @return non restituisce nulla ma ogni volta che trova un cognome che contiene l'insieme di caratteri ricercati lo stampa in console.
	 * Non trova nulla ti informa della cosa
	 */
	public void ricercaPersona(String varCognome) {
		String linea = "";
		boolean trovato = false;
		
		BufferedReader lettore = null;
		try {
			lettore = new BufferedReader(new FileReader(DataBase));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		try {
			while ((linea = lettore.readLine()) != null) {
				if ((linea.substring(40, 60).toLowerCase()).contains(varCognome.toLowerCase())) {
					System.out.println(linea);
					trovato = true;
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		if (!trovato) {
			System.out.println("Non esistono persone in anagrafica con il cognome cercato.");
		}
		
		try {
			lettore.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo per la generazione della linea da stampare all'interno del file anagrafica
	 * @param varPersona Person che si desidera inserire
	 * @return una stringa contenente tutte le informazioni della persona formattate
	 */
	private String generaLinea(Persona varPersona) {
		String linea = "";
		
		linea += varPersona.getNome().substring(0,1).toUpperCase() + varPersona.getNome().substring(1,varPersona.getNome().length()).toLowerCase();
		linea += spaziatura(varPersona.getNome(), 40);
		linea += varPersona.getCognome().substring(0,1).toUpperCase() + varPersona.getCognome().substring(1,varPersona.getCognome().length()).toLowerCase();
		linea += spaziatura(varPersona.getCognome(), 20);
		linea += varPersona.getDataNascita();
		linea += spaziatura(varPersona.getDataNascita(), 15);
		linea += varPersona.getLuogoNascita().substring(0,1).toUpperCase() + varPersona.getLuogoNascita().substring(1,varPersona.getLuogoNascita().length()).toLowerCase();
		linea += spaziatura(varPersona.getLuogoNascita(), 20);
		linea += "\n";

		return linea;
	}
	
	/**
	 * Metodo per la formattazione dei vari campi del nostro file txt. Conta i caratteri necessari per il testo da inserire e definisce il numero di caratteri da lasciare vuoti (spazi)
	 * @param var_testo testo che si vuole inserire nel campo
	 * @param larghezza_colonna larchezza in caratteri del campo che si sta analizzando
	 * @return restituisce una stringa contenente il numero di spazi necessari alla corretta formattazione del campo
	 */
	private String spaziatura(String var_testo, int larghezza_colonna) {
		String spazi = "";
		for (int i = 0; i < (larghezza_colonna - var_testo.length()); i++) {
			spazi += " ";
		}
		return spazi;
	}
}
