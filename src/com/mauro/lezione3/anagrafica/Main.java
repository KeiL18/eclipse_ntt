package com.mauro.lezione3.anagrafica;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/**
		 * gestione anagrafica / fare in modo inserire una nuova anagrafica con scanner
		 * / oppure i ricerca con un iniziale del cognome o e contenuta / comando 0
		 * termina e salva un file con tutta l anagrafica/ questo programma scrivera sul
		 * file posizionale con un certo numero di digit
		 * 
		 * nome,cognome,data di nascita, luogo di nascita, indirizzo
		 */

		Scanner interceptor = new Scanner(System.in);
		GestioneAnagrafica gestore = new GestioneAnagrafica();
		boolean inserimento_ok = true;
		boolean inserimento_utente = true;
		Persona personaTemp;
		
		System.out.println(" ---------- GESTORE ANAGRAFICA ----------\n\n"
				+ "Il programma restituisce dei file in formato .txt contenente l'anagrafica aggiornata\n"
				+ "Ogni volta che viene inserito un comando � necessario premere invio.\n\n"
				+ "---------------------------------------------------------------------------------------\n");

		while (inserimento_ok) {
			System.out.println("\n-----------------------------------------------------------------------------------------------\n"
								+ "Scegliere l'operazione che si desidera eseguire: \n" 
								+ " I - Inserire una nuova persona\n"
								+ " S - Stampa di tutte le persone presenti nella lista\n"
								+ " R - Ricercare nominativi in anagrafica tramite il cognome e visualizzare le loro informazioni\n"
								+ " Q - Terminare il programma\n");
			String inputS = (interceptor.nextLine()).trim();
			
			switch (inputS.toUpperCase()) {
			case "Q":
				inserimento_ok = false;
				System.out.println(" ---------- PROGRAMMA TERMINATO ----------");
				break;
			case "I":
				inserimento_utente = true;
				while(inserimento_utente) {
					System.out.println("\n ---------- Inserimento dati utente ----------\nInserisci il nome:");
					String inputNome = (interceptor.nextLine()).trim();
	
					if (verifica_input(inputNome, 39) && !inputNome.isBlank()) {
						System.out.println("\nInserisci il cognome:");
						String inputCognome = (interceptor.nextLine()).trim();
	
						if (verifica_input(inputCognome, 19) && !inputCognome.isBlank()) {
							System.out.println("\nInserisci la data di nascita (formato aaaa-mm-gg):\n");
							String inputDataNas = (interceptor.nextLine()).trim();
	
							if (verifica_input(inputDataNas, 10) && !inputDataNas.isBlank() && isData(inputDataNas)) {
								System.out.println("\nInserisci il luogo di nascita:\n");
								String inputLuogo = (interceptor.nextLine()).trim();
	
								if (verifica_input(inputLuogo, 19) && !inputLuogo.isBlank()) {
									personaTemp = new Persona(inputNome, inputCognome, inputDataNas, inputLuogo);
									gestore.inserimentoPersona(personaTemp);
									inserimento_utente = false;
								}
								else {
									System.out.println("\nLuogo inserito non valido (troppo lungo o non � stato inserito nulla).");
								}
							}
							else {
								System.out.println("\nData inserita non valida (inesistente o non � stato inserito nulla).");
							}
						}
						else {
							System.out.println("\nCognome inserito non valido (troppo lungo o non � stato inserito nulla).");
						}
					}
					else {
						System.out.println("\nNome inserito non valido (troppo lungo o non � stato inserito nulla).");
					}
				}
				break;
			case "S":
				gestore.stampaLista();
				break;
			case "R":
				System.out.println("\n ---------- Ricerca di un utente per cognome ---------- \nInserisci il cognome:\\n");
				String inputRicerca = (interceptor.nextLine()).trim();
				gestore.ricercaPersona(inputRicerca);
				break;
			default:
				System.out.println("\nScelta non valida!\n---------------------------------------------------------\n");
			}
		}

		interceptor.close();
		
		System.out.println("\n---------------------------------------------------------\n" + "Il programma ha terminato tutte le operazioni.\n"
				+ "\nPuoi trovare l'anagrafica eseguite qui: " + System.getProperty("user.dir"));
	}

	/**
	 * Metodo per la verifica che il valore inserito non sia troppo lungo rispetto ai campi designati all'interno del nostro "DB" anagrafica
	 * @param input valore inserito dall'utente
	 * @param caratteri lunghezza sopportata dal campo in esame
	 * @return TRUE se la lunghezza � accettabile, FALSE se � troppo lungo
	 */
	public static boolean verifica_input(String input, int caratteri) {
		boolean risultato = false;
		if (input.length() <= caratteri) {
			risultato = true;
		}
		
		return risultato;
	}
	
	/**
	 * Metodo per la verifica che sia stata inserita una data, che questa sia nel formato richiesto, che soprattutto sia una data esistente
	 * e che la persona non sia nata nel futuro(impossibile).
	 * @param varData data inserita dall'utente
	 * @return  TRUE se la data esiste, FALSE se non esiste o non � stata inserita una data
	 */
	public static boolean isData(String varData) {
		if (!varData.contains("-")) {
			System.out.println("Data non valida!");
			return false;
		}
		
		String[] campiData = varData.split("-");
		int gg = Integer.parseInt(campiData[2]);
		int mm = (Integer.parseInt(campiData[1]))-1;
		int aaaa = Integer.parseInt(campiData[0]);
		LocalDate oggi = LocalDate.now();
		LocalDate nascita = oggi.plusMonths(1);
		try {
			nascita = LocalDate.parse(varData);
		} catch (Exception ex) {
			System.out.println("La data deve avere il formato aaaa-mm-gg, quindi deve presentare uno zero se il mese o il giorno hanno una sola cifra");
			return false;
		}
		
		
		GregorianCalendar calendario = new GregorianCalendar (aaaa, mm, gg);
		calendario.setLenient(false);

		try {
			calendario.get(Calendar.DATE);
			if (nascita.isBefore(oggi)) {
				return true;
			}
			return false;
		} catch (IllegalArgumentException e) {
			System.out.println("Data non valida!");
			return false;
		}
	}
}
