package com.mauro.lezione3.anagrafica;

public class Persona {

	private String nome;
	private String cognome;
	private String luogoNascita;
	private String dataNascita;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getLuogoNascita() {
		return luogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}
	public String getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}
	
	Persona(String varNome, String varCognome, String varData, String varLuogo){
		setNome(varNome);
		setCognome(varCognome);
		setDataNascita(varData);
		setLuogoNascita(varLuogo);
	}
}
