package com.lezione22.file;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GestioneDirectory {

	public static void main(String[] args) {

//		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";
		
		//Metodo di reperimento della path relativa e trasformazione in path assoluta
//		Path currentPath = Paths.get("");
//		String currentAbsolutePath = currentPath.toAbsolutePath().toString();
//		System.out.println(currentAbsolutePath);
		
//		System.out.println(filePath);
//		File d = new File(filePath);
		
//		System.out.println(d.isFile());
//		System.out.println(d.isDirectory());
//		if(d.exists()){
//			if(d.isFile()) {									//Verifica che la path sia effettivamente un file!
//				System.out.println("File esistente");
//			}
//			else {
//				System.out.println("Attenzione hai selezionato una directory");
//			}
//		}
		
		//Lista dei file all'interno di una directory
//		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";
//		File d = new File(filePath);
//		
//		if(d.exists() && d.isDirectory()) {
//			
//			File[] lista_file = d.listFiles();
//			
//			for(int i=0; i<lista_file.length; i++) {
//				
//				String nome_file = lista_file[i].getName();
//				
//				if(lista_file[i].isDirectory()) {
//					System.out.println("DIR > " + nome_file);
//				}
//				if(lista_file[i].isFile()) {
//					System.out.println("FILE > " + nome_file);
//				}
//			}
//			
//		}
		
		//Eliminazione di tutti i file all'interno di una cartella
//		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR\\dir_da_svuotare";
//		File d = new File(filePath);
//		
//		int contatore_eliminazioni = 0;
//		if(d.exists() && d.isDirectory()) {
//			File[] lista_file = d.listFiles();
//			
//			for(int i=0; i<lista_file.length; i++) {
//				if(lista_file[i].isFile()) {
//					if(lista_file[i].delete()) {
//						contatore_eliminazioni++;
//					}
//				}
//			}
//		}
//		
//		System.out.println("Ho eliminato " + contatore_eliminazioni + " file");
		
		//Creazione nuova cartella
		String nuovaCartella = "\\test_creazione_directory";
		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";	//Base di partenza dove effettuare la creazione di una nuova directory
		System.out.println(filePath);
		
		File d = new File(filePath);
		if(d.exists() && d.isDirectory()) {
			File nd = new File(filePath + nuovaCartella);
			if(!nd.exists()) {
				nd.mkdir();
				System.out.println("Cartella creata con successo");
			}
			else {
				System.out.println("Cartella gi� esistente!");
			}
		}
	}
}
