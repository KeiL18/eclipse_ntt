package com.lezione22.file.concessionario;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		String filePath = System.getProperty("user.dir");
		File d = new File(filePath);
		String nuovaCartella = "\\gestione_concessionario";
		String nomeFile = "Concessionario.txt";
		String[] elenco = {"Fiat 500", "Ford Fiesta", "Skoda Octavia"};
		
		
		if(d.exists() && d.isDirectory() && d.canWrite()) {
			File nuovo_file = new File(filePath + nuovaCartella + nomeFile);
			
			try {
				if(nuovo_file.createNewFile()) {
					System.out.println("File creato con successo!");
				}
				else {
					System.out.println("File già esistente!");
				}
				
			FileWriter scrittore = null;
			scrittore = new FileWriter(nuovo_file);
			for(int i = 0; i <elenco.length; i++) {
				scrittore.write(elenco[i] + "\n");
			}
			
			scrittore.close();
			
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
