package com.lezione22.file.homework;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		/**
		 * Creare un software di gestione scolastica per un istito elementare.
		 * La suddivisione delle aule segue la seguente struttura:
		 * 
		 * SEZIONE: Con lettera da A ad F
		 * CLASSI: Da 1 a 5
		 * Es: Giovanni Pace della 3B, Mario Rossi della 5F
		 * 
		 * Voglio che:
		 * - All'inserimento di un nuovo studente, i dettagli vengano salvati all'interno della corretta
		 * combinazione SEZIONE-CLASSE in un file che ha il nome della classe e directory relativa alla sezione.
		 * - All'inizio di ogni file, inserire una stringa con su scritto la classe e sezione di riferimento.
		 * Es: REGISTRO 3B
		 */
		
		GestioneStudenti gestore = new GestioneStudenti();
		Scanner interceptor = new Scanner(System.in);
		boolean inserimento_ok = true;
		boolean inserimento_utente = true;
		
		gestore.aggiungiStudente("Mario", "Rossi", "s123456", "3B");
		gestore.aggiungiStudente("Carlo", "Verdi", "s123457", "4B");
		gestore.aggiungiStudente("Dario", "Rolli", "s123458", "2A");
		gestore.aggiungiStudente("Andrea", "Luca", "s123459", "1F");
		gestore.aggiungiStudente("Giorgia", "Ferrari", "s123450", "5C");
		
		System.out.println(" ---------- GESTORE ANAGRAFICA ----------\n\n"
				+ "---------------------------------------------------------------------------------------\n");

		while (inserimento_ok) {
			System.out.println("\n-----------------------------------------------------------------------------------------------\n"
								+ "Scegliere l'operazione che si desidera eseguire: \n" 
								+ " I - Inserire un nuovo studente\n"
								+ " Q - Terminare il programma\n");
			String inputS = (interceptor.nextLine()).trim();
			
			switch (inputS.toUpperCase()) {
			case "Q":
				inserimento_ok = false;
				System.out.println(" ---------- PROGRAMMA TERMINATO ----------");
				break;
			case "I":
				inserimento_utente = true;
				while(inserimento_utente) {
					System.out.println("\n ---------- Inserimento dati utente ----------\nInserisci il nome:");
					String inputNome = (interceptor.nextLine()).trim();
	
					if (verifica_input(inputNome, 39) && !inputNome.isBlank()) {
						System.out.println("\nInserisci il cognome:");
						String inputCognome = (interceptor.nextLine()).trim();
	
						if (verifica_input(inputCognome, 19) && !inputCognome.isBlank()) {
							System.out.println("\nInserisci la matricola:\n");
							String inputMatricola = (interceptor.nextLine()).trim();
	
							if (verifica_input(inputMatricola, 10) && !inputMatricola.isBlank() && !inputMatricola.contains(" ")) {
								System.out.println("\nInserisci la classe:\n");
								String inputClasse = (interceptor.nextLine()).trim();
	
								if (inputClasse.length() == 2 && !inputClasse.isBlank() && !inputClasse.contains(" ")) { //servono?
									gestore.aggiungiStudente(inputNome, inputCognome, inputMatricola, inputClasse);
									inserimento_utente = false;
								}
								else {
									System.out.println("\nClasse non valida (troppo lungo o non � stato inserito nulla).");
								}
							}
							else {
								System.out.println("\nMatricola non valida (troppo lungo o non � stato inserito nulla).");
							}
						}
						else {
							System.out.println("\nCognome inserito non valido (troppo lungo o non � stato inserito nulla).");
						}
					}
					else {
						System.out.println("\nNome inserito non valido (troppo lungo o non � stato inserito nulla).");
					}
				}
				break;
			default:
				System.out.println("\nScelta non valida!\n---------------------------------------------------------\n");
			}
		}
		interceptor.close();
		
		System.out.println("\n---------------------------------------------------------\n" + "Il programma ha terminato tutte le operazioni.\n"
				+ "\nPuoi trovare la cartella qui: " + System.getProperty("user.dir"));
	}

	/**
	 * Metodo per la verifica che il valore inserito non sia troppo lungo rispetto ai campi designati all'interno del nostro "DB" anagrafica
	 * @param input valore inserito dall'utente
	 * @param caratteri lunghezza sopportata dal campo in esame
	 * @return TRUE se la lunghezza � accettabile, FALSE se � troppo lungo
	 */
	public static boolean verifica_input(String input, int caratteri) {
		boolean risultato = false;
		if (input.length() <= caratteri) {
			risultato = true;
		}
		
		return risultato;
	}

}
