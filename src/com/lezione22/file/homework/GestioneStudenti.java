package com.lezione22.file.homework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GestioneStudenti {
	
	private final String nuovaCartella = File.separator + "gestione_scolastica";
	private final String filePath = System.getProperty("user.dir") + nuovaCartella;
	
	public void aggiungiStudente(String varNome, String varCognome, String varMatricola, String varClasse) {
		Studente studTemp = new Studente(varNome, varCognome, varMatricola, varClasse);
				
		String dir = filePath;
		File Fdir = new File(dir);
		if (!Fdir.exists())
			Fdir.mkdir();
		
		if(studenteEsiste(studTemp)) {
			System.out.println("Studente già inserito (omonimo o matricola già esistente)");
			return;
		}
		
		dir = filePath + File.separator + studTemp.getSezione();
		String fileDaScrivere =  dir + File.separator + studTemp.getClasse() + ".txt";
		File directory = new File(dir);
		if (!directory.exists())
			directory.mkdir();
		
		File fileTemp = new File(fileDaScrivere);
		if(fileTemp.exists()) {
			FileWriter scrittore;
			try {
				scrittore = new FileWriter(fileTemp, true);
				scrittore.write(studTemp.stringaStud());
				scrittore.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			FileWriter scrittore;
			try {
				scrittore = new FileWriter(fileTemp, false);
				scrittore.write("REGISTRO " + studTemp.getClasse() + studTemp.getSezione() + "\n\n");
				scrittore.write(studTemp.stringaStud());
				scrittore.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
	}
	
	private boolean studenteEsiste(Studente varStud) {
		String percorso = filePath + File.separator + varStud.getSezione();
		File per = new File(percorso);
		if(!per.exists())
			return false;
		
		percorso += File.separator + varStud.getClasse() + ".txt";
		per = new File(percorso);
		if(!per.exists())
			return false;
		
		if(ricercaPersonaMatr(varStud.getMatricola(), percorso) || ricercaPersonaNom(varStud.getNome(), percorso))
			return true;	
		
		return false;
	}
	
	private boolean ricercaPersonaMatr(String varMatricola, String varFile) {
		String linea = "";
		boolean trovato = false;
		
		BufferedReader lettore = null;
		try {
			lettore = new BufferedReader(new FileReader(varFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		try {
			lettore.readLine(); //salto le prime due righe di intestazione
			lettore.readLine();
			while ((linea = lettore.readLine()) != null) {
				String temp[] = linea.split(",");
				if (temp[2].equals(varMatricola.toLowerCase())) {
					trovato = true;
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		try {
			lettore.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return trovato;
	}
	
	private boolean ricercaPersonaNom(String varNome, String varFile) {
		String linea = "";
		boolean trovato = false;
		
		BufferedReader lettore = null;
		try {
			lettore = new BufferedReader(new FileReader(varFile));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		try {
			lettore.readLine(); //salto le prime due righe di intestazione
			lettore.readLine();
			while ((linea = lettore.readLine()) != null) {
				String temp[] = linea.split(",");
				if (temp[0].toLowerCase().trim().equals(varNome.toLowerCase())) {
					trovato = true;
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		try {
			lettore.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return trovato;
	}
}

