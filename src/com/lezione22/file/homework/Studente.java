package com.lezione22.file.homework;

public class Studente {
	
	private String nome;
	private String cognome;
	private String matricola;
	private String sezione;
	private int classe;
	
	Studente(String varNome, String varCognome, String varMatricola, String varClasse){
		this.setClasse(Integer.parseInt(varClasse.substring(0,1)));
		this.setCognome(varCognome);
		this.setMatricola(varMatricola.toLowerCase());
		this.setNome(varNome);
		this.setSezione(varClasse.substring(1).toUpperCase());
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public String getSezione() {
		return sezione;
	}
	public void setSezione(String sezione) {
		this.sezione = sezione;
	}
	public int getClasse() {
		return classe;
	}
	public void setClasse(int classe) {
		this.classe = classe;
	}
	
	/**
	 * Metodo per la generazione della linea da stampare all'interno del file anagrafica
	 * @param varPersona Person che si desidera inserire
	 * @return una stringa contenente tutte le informazioni della persona formattate
	 */
	public String stringaStud() {
		String linea = "";
		
		linea += this.getNome().substring(0,1).toUpperCase() + this.getNome().substring(1,this.getNome().length()).toLowerCase() + ",";
		linea += this.getCognome().substring(0,1).toUpperCase() + this.getCognome().substring(1,this.getCognome().length()).toLowerCase() + ",";
		linea += this.getMatricola();
		linea += "\n";

		return linea;
	}
}
