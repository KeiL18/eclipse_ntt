package com.lezione22.file;

import java.io.File;
import java.io.IOException;

public class GestioneFileWriter {

	public static void main(String[] args) {

		//Creazione di un nuovo file
		String nomeFile = "\\esempio.txt";
		String filePath = System.getProperty("user.home") + "\\Desktop\\FWFR";
		
		File d = new File(filePath);
		if(d.exists() && d.isDirectory() && d.canWrite()) {
			
			File nuovo_file = new File(filePath + nomeFile);
			
			try {
				if(nuovo_file.createNewFile()) {
					System.out.println("File creato con successo!");
				}
				else {
					System.out.println("File gi� esistente!");
				}

				
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			
		}
	}

}
