package com.lezione17.polimorfismo.gestione_preventivi_soluzione;

public class Voce {

	private String nome;
	private String codice;
	private float quant;
	private float prezzo;
	private float sconto; 
	private float iva;
	private float totale;
	
	Voce(String nome, String codice, float quant, float prezzo, float sconto, float iva) {
		this.nome = nome;
		this.codice = codice;
		this.quant = quant;
		this.prezzo = prezzo;
		this.sconto = sconto;
		this.iva = iva;
		this.totale = calcolaTotale();
	}
	
	private float calcolaTotale() {
		float valore_iniziale = this.prezzo * this.quant;
		float valore_sconto = (valore_iniziale / 100) * this.sconto;
		float valore_scontato = valore_iniziale - valore_sconto;
		float valore_ivato = valore_scontato * ((iva/100) + 1);
		return valore_ivato;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public float getQuant() {
		return quant;
	}
	public void setQuant(float quant) {
		this.quant = quant;
		this.totale = this.calcolaTotale();				//Ricalcolo totale
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
		this.totale = this.calcolaTotale();				//Ricalcolo totale
	}
	public float getSconto() {
		return sconto;
	}
	public void setSconto(float sconto) {
		this.sconto = sconto;
		this.totale = this.calcolaTotale();				//Ricalcolo totale
	}
	public float getIva() {
		return iva;
	}
	public void setIva(float iva) {
		this.iva = iva;
		this.totale = this.calcolaTotale();				//Ricalcolo totale
	}
	public float getTotale() {
		return totale;
	}
	public void setTotale(float totale) {
		this.totale = totale;
	}

	@Override
	public String toString() {
		return "Voce [nome=" + nome + ", codice=" + codice + ", quant=" + quant + ", prezzo=" + prezzo + ", sconto="
				+ sconto + ", iva=" + iva + ", totale=" + totale + "]";
	}
	
	
	
	
	
}