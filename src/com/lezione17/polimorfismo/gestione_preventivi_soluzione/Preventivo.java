package com.lezione17.polimorfismo.gestione_preventivi_soluzione;

import java.util.ArrayList;

public class Preventivo {
	
	public static int contatore_preventivi = 0;

	private String data_prev;
	private String codice_prev;
	private Indirizzo spedizione;
	private Indirizzo fatturazione;
	private ArrayList<Voce> elenco_voci = new ArrayList<Voce>();	//Lo creo di default così da non doverlo inizializzare in ogni costruttore.
	
	Preventivo(String var_data){
		String[] data_spezzata = var_data.split("-");
		
		this.data_prev = var_data;
		this.codice_prev = contatore_preventivi + 1 + "/" + data_spezzata[0];
		contatore_preventivi++;
	}
	
	public String getData_prev() {
		return data_prev;
	}
	public void setData_prev(String data_prev) {
		this.data_prev = data_prev;
	}
	public String getCodice_prev() {
		return codice_prev;
	}
	public void setCodice_prev(String codice_prev) {
		this.codice_prev = codice_prev;
	}
	public Indirizzo getSpedizione() {
		return spedizione;
	}
	public void setSpedizione(Indirizzo spedizione) {
		this.spedizione = spedizione;
	}
	public Indirizzo getFatturazione() {
		return fatturazione;
	}
	public void setFatturazione(Indirizzo fatturazione) {
		this.fatturazione = fatturazione;
	}
	public ArrayList<Voce> getElenco_voci() {
		return elenco_voci;
	}
	public void setElenco_voci(ArrayList<Voce> elenco_voci) {
		this.elenco_voci = elenco_voci;
	}
	
	/**
	 * Funzione personalizzata per l'aggiunta della voce al preventivo
	 * @param obj_voce
	 */
	public void addVoceSpesa(Voce obj_voce) {
		this.elenco_voci.add(obj_voce);
	}
}
