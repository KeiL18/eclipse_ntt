package com.lezione17.polimorfismo.gestione_preventivi;

import java.util.ArrayList;

public class Fornitore extends MemoriaFornitori{
	
	protected String fornitore;
	protected ArrayList<Preventivo> array_preventivi = new ArrayList<Preventivo>();
//	private ArrayList<Indirizzo> indirizzo_fornitore = new ArrayList<Indirizzo>();
	
	public void setFornitore (String var_for) {
		this.fornitore = var_for;
	}
	
	public String getFornitore () {
		return this.fornitore;
	}
	
	public void addPreventivo(Preventivo obj_preventivo) {
		this.array_preventivi.add(obj_preventivo);
	}
	
	public void stampaPreventivoFornitore (String var_cod) {
		for (Preventivo oggetto: array_preventivi) {
			oggetto.stampaPreventivo1();
			System.out.println("\n");
		}
	}
	
}
