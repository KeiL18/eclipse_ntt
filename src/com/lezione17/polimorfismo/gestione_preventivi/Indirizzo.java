package com.lezione17.polimorfismo.gestione_preventivi;

public class Indirizzo extends Preventivo{
	
	private String stato;
	private String paese;
	private String via;
	private String numero;
	
	public void setStato(String var_sta) {
		this.stato = var_sta;
	}
	
	public void setPaese(String var_pae) {
		this.paese = var_pae;
	}
	
	public void setVia(String var_via) {
		this.via = var_via;
	}
	
	public void setNumero(String var_num) {
		this.numero = var_num;
	}
	
	public String getStato() {
		return this.stato;
	}
	
	public String getPaese() {
		return this.paese;
	}
	
	public String getVia() {
		return this.via;
	}
	
	public String getNumero() {
		return this.numero;
	}
	
	public void stampaIndirizzo() {
		System.out.println("Via " + this.via + " " + this.numero + ", " + this.paese + ", " + this.stato);
	}
}
