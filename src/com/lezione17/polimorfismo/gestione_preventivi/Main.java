package com.lezione17.polimorfismo.gestione_preventivi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
	
	
	public static void main(String[] args) throws ParseException {
		/*
		  Un preventivo � caratterizzato da:

			- Data del preventivo
			- Codice del preventivo
			- Indirizzo del Fornitore (campo composto)
			- Indirizzo del Cliente (campo composto)
			- Elenco delle voci di spesa:
			    - Nome
			    - Codice
			    - Quantit�
			    - Prezzo
			    - Sconto
			    - IVA
			    - Totale (calcolato in automatico)
		*/

		boolean esecuzione = true;
		String operazione;
		MemoriaFornitori memoria_1 = new MemoriaFornitori();
		
		while (esecuzione) {
			Scanner letturaOperazione = new Scanner(System.in);
			
			System.out.println(" ---------- NUOVA OPERAZIONE ----------\n"
					+ "Selezionare l'operazione che si desidera eseguire:\n"
				+ " AP - Aggiungere preventivo\n"
				+ " AF - Aggiungere un nuovo fornitore\n"
				+ " MP - Modifica preventivo\n"
				+ " MF - Modifica fornitore\n"
				+ " C - Consultare un preventivon\n"
				+ " Q - Uscire dal programma");
			
			operazione = letturaOperazione.nextLine();
			letturaOperazione.close();
			esecuzione = esecuzioneOperazioni (operazione, memoria_1);
		}
		
		
		Preventivo pre_1 = new Preventivo();
		pre_1.setCliente("Andrea");
		pre_1.setCodice("01111");
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		Date var_dat = ft.parse("2021-02-13");
		pre_1.setData(var_dat);
		
		Fornitore for_1 = new Fornitore();
		for_1.setFornitore("AB345");
		for_1.addPreventivo(pre_1);
		memoria_1.addFornitore(for_1);
		
		String ric_cod;
		Scanner interceptor = new Scanner(System.in);
		ric_cod = interceptor.nextLine();
		for_1.stampaPreventivoFornitore(ric_cod);
		
		interceptor.close();
		
	}
	
	
	
	public boolean esecuzioneOperazioni (String var_ope, MemoriaFornitori var_mem){
		boolean var_ese = true;
		Scanner letturaInserimenti = new Scanner(System.in);
		
		var_ope = var_ope.trim().toUpperCase();
		String var_input;
		
		switch (var_ope) {
			case "AP":
				System.out.println("A quale fornitore si desidera aggiungere un preventivo?");
				System.out.println("Lista fornitori:");
				var_mem.stampaListaFornitori();
				var_input = letturaInserimenti.nextLine();
				aggiungiPreventivo(var_input);
				break;
			case "AF":
				
				break;
			case "MP":
				
				break;
			case "MF":
				
				break;
			case "C":
				
				break;
			case "Q":
				var_ese = false;
				break;
			default:
				System.out.println("[ERR] Comando non valido.");
		}
		letturaInserimenti.close();
		return var_ese;
	}

	public void aggiungiPreventivo(String var) {
		Scanner letturaInput = new Scanner(System.in);
		Preventivo pre_temp = new Preventivo();
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		String var_in;
		boolean ok = false;
		
		System.out.println("Inserire il cliente:");
		var_in = letturaInput.nextLine();
		pre_temp.setCliente(var_in);
		
		
		while (ok){
			System.out.println("Inserire il codice preventivo:");
			var_in = letturaInput.nextLine();
//			for ()
		}
		pre_temp.setCodice(var_in);
		
		System.out.println("Inserire la data: [aaaa/mm/gg]");
		var_in = letturaInput.nextLine();
		SimpleDateFormat ft2 = new SimpleDateFormat ("yyyy-MM-dd");
		Date var_dat = ft2.parse(var_in);
		pre_temp.setData(var_dat);
		
		letturaInput.close();
	}
}
