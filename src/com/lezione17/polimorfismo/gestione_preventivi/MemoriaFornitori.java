package com.lezione17.polimorfismo.gestione_preventivi;

import java.util.ArrayList;

public class MemoriaFornitori{

	protected ArrayList<Fornitore> array_fornitori = new ArrayList<Fornitore>();
	
	public void addFornitore(Fornitore obj_fornitore) {
		this.array_fornitori.add(obj_fornitore);
	}
	
	public void stampaListaFornitori() {
		for(Fornitore oggetto: array_fornitori) {
			System.out.println(" - " + oggetto.getFornitore() + "\n");
		}
	}
	
	public int ricercaFornitore(String var_ric) {
		for(int i = 0; i < array_fornitori.size(); i++) {
			if(array_fornitori[i].getFornitore() == var_ric) {
				return i;
			}
		}
	}
	
}
