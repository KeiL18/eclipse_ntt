package com.lezione17.polimorfismo.gestione_preventivi;

import java.util.ArrayList;
import java.util.Date;

public class Preventivo extends Fornitore{

	protected String cliente;
	protected Date data_prev;
	protected String codice;
	protected ArrayList<Indirizzo> indirizzo_cliente = new ArrayList<Indirizzo>();
	protected ArrayList<VoceSpesa> array_voci_spesa = new ArrayList<VoceSpesa>();
	protected float totale;
	
	public void setCliente(String var_cli) {
		this.cliente = var_cli;
	}
	
	public void setData(Date var_dat) {
		this.data_prev = var_dat;
	}
	
	public boolean setCodice(String var_cod) {
		this.codice = var_cod;
	}
	
	public void setIndirizzoCliente (ArrayList<Indirizzo> obj_indCli) {
		this.indirizzo_cliente = obj_indCli;
	}
	
	public void setTotale() {
		//TODO: generare la somma automatica
	}
	
	public String getCliente() {
		return this.cliente;
	}
	
	public Date getData() {
		return this.data_prev;
	}
	
	public String getCodice() {
		return this.codice;
	}
	
	public ArrayList<Indirizzo> getIndirizzoCliente(){
		return this.indirizzo_cliente;
	}
	
	public float getTotale() {
		return this.totale;
	}
	
	VoceSpesa voce_1 = new VoceSpesa();
			
	voce_1.setArticolo("Olio");
	voce_1.setCD("10CH11");
	
	
	
	public void stampaPreventivo1() {
		System.out.println("-----------------------------------------" +
				"Cliente: " +  this.cliente + "\n" +
				"Data: " +  this.data_prev + "\n" +
//				"Indirizzo fornitore: " +  stampaIndirizzo(#####) + "\n" +
//				"Indirizzo cliente: " +  stampaIndirizzo(#####) + "\n" +
//				stampaVoce() +
				"Totale di spesa: " +  this.totale + "\n" +
				"-----------------------------------------" 
				 );
	}
}
