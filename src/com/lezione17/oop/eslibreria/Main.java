package com.lezione17.oop.eslibreria;

public class Main {

	public static void main(String[] args) {
		
		/**
		* Creare il sistema di gestione di una libreria che mantiene all'interno
		* della propria struttura un numero indefinito di libri e dispositivi video.
		*/
		
		Libro lib_1 = new Libro();
		lib_1.setAutore("Giovanni Pace");
		lib_1.setIsbn("123456789");
		lib_1.setTitolo("La programmazione");
		
		Libro lib_2 = new Libro();
		lib_2.setAutore("Mario Rossi");
		lib_2.setIsbn("987654321");
		lib_2.setTitolo("La filosofia");
		
		DispositivoVideo vid_1 = new DispositivoVideo();
		vid_1.setIsbn("5464654654654");
		vid_1.setTitolo("Il signore degli anelli");
		
		Libreria centrale = new Libreria();
		centrale.addSupporto(lib_1);
		centrale.addSupporto(lib_2);
		centrale.addSupporto(vid_1);
		centrale.stampaCatalogoFE();
	}

}
