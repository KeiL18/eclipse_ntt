package com.lezione17.oop.eslibreria;

public abstract class Supporto {
		
	protected String isbn;
	protected String titolo;
	
	public void setIsbn (String var_isbn) {
		this.isbn = var_isbn;
	}
	
	public void setTitolo (String var_tit) {
		this.titolo = var_tit;
	}
	
	public String getIsbn () {
		return this.isbn;
	}
	
	public String getTitolo () {
		return this.titolo;
	}
	
	public void stampaDettagli() {
		System.out.println(this.isbn + " - " + this.titolo);
	}
}
