package com.lezione17.oop.eslibreria;

public class Libro extends Supporto{
	
	private String autore;

	public void setAutore (String var_aut) {
		this.autore = var_aut;
	}
	
	public String getAutore () {
		return this.autore;
	}
	
	@Override
	public void stampaDettagli() {
		System.out.println(super.isbn + " - " + super.titolo + " - " + this.autore);
	}
}
