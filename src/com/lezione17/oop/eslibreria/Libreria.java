package com.lezione17.oop.eslibreria;

import java.util.ArrayList;

public class Libreria {

	private ArrayList<Supporto> array_supporti = new ArrayList<Supporto>();
	
	public void addSupporto(Supporto obj_Supporto) {
		this.array_supporti.add(obj_Supporto);
	}
	
	public void stampaCatalogo () {
		for (int i = 0; i <this.array_supporti.size(); i++) {
			Supporto sup_temp = this.array_supporti.get(i);
			
			sup_temp.stampaDettagli();
			//System.out.println(sup_temp.getIsbn() + " - " + sup_temp.getTitolo());
		}
	}
	
	public void stampaCatalogoFE( ) {
		for (Supporto oggetto: array_supporti) {
			System.out.println(oggetto.getClass().getName());	//prendo il nome della classe con annesso il package
			oggetto.stampaDettagli();
			System.out.println("\n");
		}
	}
	
//	public ArrayList<Libro> getElencoLibri() {
//		
//	}
}
