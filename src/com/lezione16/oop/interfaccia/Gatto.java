package com.lezione16.oop.interfaccia;

public class Gatto implements Animale{

    @Override
    public void verso() {
        System.out.println("Meow Meow");
    }

    @Override
    public void movimento() {
        System.out.println("Cammina");
    }

}
