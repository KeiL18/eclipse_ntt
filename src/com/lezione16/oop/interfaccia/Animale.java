package com.lezione16.oop.interfaccia;

public interface Animale {

    void verso();
    void movimento();

}
