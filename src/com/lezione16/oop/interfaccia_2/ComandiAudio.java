package com.lezione16.oop.interfaccia_2;

public interface ComandiAudio {

    void play();
    void pausa();
    void stop();

}

