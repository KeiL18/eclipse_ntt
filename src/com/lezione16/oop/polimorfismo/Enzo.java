package com.lezione16.oop.polimorfismo;

public class Enzo extends Ferrari{

    private int numero_porte = 3;

    @Override
    public void coloreSelezionato() {
        System.out.println("Nero");
    }

    public int getNumeroPorte() {
        return this.numero_porte;
    }

}
