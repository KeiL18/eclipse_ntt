package com.lezione16.oop.associazioni.onetoone.esercizio;

public class Etichetta {
	
	private String codice;
	private String scaffale;
	private int num_posizione;
	private String categoria;
	
	Etichetta(){
		
	}
	
	Etichetta(String var_cod, String var_sca, int var_pos, String var_cat){
		this.codice = var_cod;
		this.scaffale = var_sca;
		this.num_posizione = var_pos;
		this.categoria = var_cat;
	}
	
	public void setCodice(String var_cod) {
		this.codice = var_cod;
	}
	
	public void setScaffale(String var_sca) {
		this.scaffale = var_sca;
	}
	
	public void setPosizione(int var_pos) {
		this.num_posizione = var_pos;
	}
	
	public void setCategoria (String var_cat) {
		this.categoria = var_cat;
	}
	
	public String getcCodice() {
		return this.codice;
	}
	
	public String getScaffale() {
		return this.scaffale;
	}
	
	public int getPosizione() {
		return this.num_posizione;
	}
	
	public String getcategoria() {
		return this.categoria;
	}
}
