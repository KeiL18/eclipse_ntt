package com.lezione16.oop.associazioni.onetoone.esercizio;

public class Libro {
		
		private String titolo;
		private String autore;
		private Etichetta rif_etic;
		
		Libro (){
			
		}
		
		Libro(String var_tit, String var_aut, Etichetta obj_cod){
			this.titolo = var_tit;
			this.autore = var_aut;
			this.rif_etic = obj_cod;
		}
		
		public void setTitolo(String var_tit) {
			this.titolo = var_tit;
		}
		
		public void setAutore(String var_aut) {
			this.autore = var_aut;
		}
		
		public void setEtichetta(Etichetta obj_cod) {
			this.rif_etic = obj_cod;
		}
		
		public String getTitolo() {
			return this.titolo;
		}
		
		public String getAutore() {
			return this.autore;
		}
		
		public Etichetta getRifEtichetta() {
			return this.rif_etic;
		}
}
