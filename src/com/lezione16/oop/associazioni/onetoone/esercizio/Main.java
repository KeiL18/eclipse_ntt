package com.lezione16.oop.associazioni.onetoone.esercizio;

public class Main {

	public static void main(String[] args) {
		
		/*
		 * Creare un sistema che colleghi un libro ad una
		 * ed una sola etichetta!
		 * 
		 * E se volessi associare un'etichetta anche ad una
		 * rivista ed un fumetto?
		 */
		
		Etichetta eti_1 = new Etichetta();
		eti_1.setScaffale("E");
		eti_1.setPosizione(2);
		eti_1.setCategoria("Storia");
		
		Libro lib_1 = new Libro();
		lib_1.setTitolo("L'ascesa degli Unni");
		lib_1.setAutore("Boh");
		lib_1.setEtichetta(eti_1);
		
		System.out.println(eti_1);
	}

}
