package com.lezione16.oop.associazioni.onetoone;

public class CodiceFiscale {
	
	private String codice;
	private String data_scad;
	private Persona per_rif;
	
	CodiceFiscale(){
		
	}
	
	CodiceFiscale(String var_cod, String var_dat){
		this.codice = var_cod;
		this.data_scad = var_dat;
	}
	
	public void setCodiceFiscale(String var_cod) {
		this.codice = var_cod;
	}
	public void setDataScadenza(String var_dat) {
		this.data_scad = var_dat;
	}
	
	public String getCodiceFiscale() {
		return this.codice;
	}
	public String getDataScadenza() {
		return this.data_scad;	
	}
	
	public void setPersonaRif(Persona obj_pers) {
		this.setPersonaRif(obj_pers);
	}
	public Persona getPersonaRif() {
		return this.per_rif;
	}
	
	public void stampaCodiceFiscale() {
		System.out.println("Codice: " + this.codice + "\nScadenza: " + this.data_scad);
	}
	
}
