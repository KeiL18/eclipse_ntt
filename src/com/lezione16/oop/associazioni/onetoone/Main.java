package com.lezione16.oop.associazioni.onetoone;

public class Main {

	public static void main(String[] args) {

		Persona per_1 = new Persona("Giovanni Pace");
		CodiceFiscale cf_1 = new CodiceFiscale("7894567896", "2020-02-01");
		
		per_1.setCodiceFiscale(cf_1);
		cf_1.setPersonaRif(per_1);
		
		per_1.stampaDettaglio();
	}

}